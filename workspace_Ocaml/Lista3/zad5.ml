let insertionsort pred xs =
	let rec insert element list = 
		match list with
		[] -> [element]
		| h :: t as list -> if (pred h element) then h :: (insert element t)
													else element :: list
	in List.fold_left (fun acc element -> insert element acc) [] xs;;
	
let isSmaller a b = fst a <= fst b;;

insertionsort isSmaller [(0,0);(2,0); (0,1); (-1,9)];;

let insertionsort f order xs=
	let xs = List.rev xs in
	let rec insert value = function
		| [] -> [value]
		| h::t (*-> if f order value h then value::h::t*) (* :: - konstruktor listy, lacznosc prawostronna, z lewej el z prawej lista*)
						 as sortedl-> if f order value h then value::sortedl
								else h::insert value tl
	in let sort = function
		| [] -> []
		| h::t -> insert h (sort t)
	in sort xs;;

let rec mergesort order list =
	let partition = List.length list / 2 in
		if partition == 0 then list
		else
			let rec merge = function
				[], list2 -> list2
				| list1, [] -> list1
				| head1 :: tail1 as list1, (head2 :: tail2 as list2) ->
								if order head1 head2 then head1 :: merge(tail1, list2)
								else head2 :: merge(list1, tail2)
			and split leftSublist rightSublist counter =
					if counter = 0 then (List.rev leftSublist, rightSublist)
					else split (List.hd rightSublist :: leftSublist) (List.tl rightSublist) (counter - 1)
			in let (left, right) = split [] list partition
		in merge(mergesort order left, mergesort order right);;

mergesort isSmaller [(0,2);(2,0); (0,1); (-1,9)];;