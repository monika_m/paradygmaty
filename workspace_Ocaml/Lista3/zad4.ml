(*a)*)
 let rec quicksort = function            
	|  []  -> []          
	|  [x] -> [x]          
	|  xs  -> let small = List.filter (fun y -> y < List.hd xs ) xs         
	           and large = List.filter (fun y -> y >= List.hd xs ) xs          
						          in quicksort small @ quicksort large;;  

		(*Poniewaz w momencie, gdy lista ma wiecej niz jeden element i nie ma mniejszych
elementow niz pierwszy, przestanie ona sie dzielic i funkcja bedzie sie ciagle wywolywac
rekurencyjnie dla tej samej listy, az nastapi stack overflow.
posortuje dla posortowanej malejaco O(n2)*)  
          
(*b)*)
let rec quicksort' = function      
	|  [] -> []         
	|  x::xs -> let small = List.filter (fun y -> y < x ) xs         
	            and large = List.filter (fun y -> y > x ) xs         
							            in quicksort' small @ (x :: quicksort' large);;

(*Jesli jakis element sie powtarza, to w liscie wynikowej bedzie wystepowal tylko raz.*)