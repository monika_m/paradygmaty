package pp.Laby11;


import rx.Observer;
public class Consumer<T> implements Observer<T> {

	public void onCompleted() {
		System.out.println("Done");
	}

	public void onError(Throwable e) {
		System.out.println("Error! " + e.getMessage());
		
	}

	public void onNext(T t) {
		System.out.println("Producer takes "+ t);
		
	}

}
