package pp.Laby11;

import rx.Observable;

class Producer {
	
	private Consumer<String> cons;
	
	public Producer(Consumer<String> cons) {
		super();
		this.cons=cons;
	}
	
	public void produce(int msgCount){
		String[] messages= new String[msgCount];
		for (int i=0; i<msgCount; i++) {
			messages[i]="m" + i;
		}
		Observable.from(messages).subscribe(cons);
	}
}
