package zrodla;

public class Dokument {

	private int liczbaStron;
	private String nazwa;
	private double zapelnienie=1;
	
	
	public int getLiczbaStron() {
		return liczbaStron;
	}

	public Dokument() {
		liczbaStron=1;
	}

	public Dokument(int liczbaStron) {
		this.liczbaStron = liczbaStron;
	}
	
	public Dokument(int liczbaStron, String nazwa) {
		this.liczbaStron = liczbaStron;
		this.nazwa = nazwa;
	}

	public Dokument(int liczbaStron, String nazwa, double zapelnienie) {
		this.nazwa = nazwa;
		this.zapelnienie=zapelnienie;
		this.liczbaStron= (int)(liczbaStron*zapelnienie);
	}

	@Override
	public String toString() {
		return "Dokument "+nazwa+" liczba stron: "+ liczbaStron;
	}

	public String getNazwa() {
		return nazwa;
	}

}
