package drukarka;

import materialy_eksploatacyjne.BlokPapieru;
import materialy_eksploatacyjne.WkladDrukujacy;
import zrodla.Dokument;

public class UrzadzenieWielofunkcyjne extends DrukarkaKolorowa implements ZeSkanerem, Bezprzewodowa{

	
	public UrzadzenieWielofunkcyjne(boolean czyLaserowa) {
		super(czyLaserowa);
	}

	public UrzadzenieWielofunkcyjne(BlokPapieru zasobnik, WkladDrukujacy wklad, WkladDrukujacy wkladKol) {
		super(zasobnik, wklad, wkladKol);
	}

	public UrzadzenieWielofunkcyjne() {
		super();
	}


	@Override
	public boolean skonfigurujPolaczenie(String nazwa_sieci, String haslo) {
		if (Math.random() < 0.5) {
			this.port = "Wifi: " + nazwa_sieci;
			return true;
		} else {
			return false;
		}
	}

	

}
