package drukarka;

import materialy_eksploatacyjne.BlokPapieru;
import materialy_eksploatacyjne.WkladDrukujacy;

public class DrukarkaBezprzewodowa extends Drukarka implements Bezprzewodowa {

	public DrukarkaBezprzewodowa() {
		super();
	}

	public DrukarkaBezprzewodowa(boolean czyLaserowa) {
		super(czyLaserowa);
	}

	public DrukarkaBezprzewodowa(BlokPapieru zasobnik, WkladDrukujacy wklad) {
		super(zasobnik, wklad);
	}

	@Override
	public boolean skonfigurujPolaczenie(String nazwa_sieci, String haslo) {
		if (Math.random() < 0.5) {
			this.port = "Wifi: " + nazwa_sieci;
			return true;
		} else {
			return false;
		}

	}

}
