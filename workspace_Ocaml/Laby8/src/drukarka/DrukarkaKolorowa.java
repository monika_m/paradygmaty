package drukarka;

import materialy_eksploatacyjne.BlokPapieru;
import materialy_eksploatacyjne.Kolor;
import materialy_eksploatacyjne.Toner;
import materialy_eksploatacyjne.Tusz;
import materialy_eksploatacyjne.WkladDrukujacy;
import user_interface.InterfaceHelpers;
import zrodla.Dokument;

public class DrukarkaKolorowa extends Drukarka{
	
	private WkladDrukujacy wkladKol;
	
	public DrukarkaKolorowa(BlokPapieru zasobnik, WkladDrukujacy wklad, WkladDrukujacy wkladKol) {
		super(zasobnik, wklad);
		this.wkladKol=wkladKol;
	}


	public DrukarkaKolorowa(boolean czyLaserowa) {
		super(czyLaserowa);
		if (czyLaserowa)
			this.wkladKol=new Toner(Kolor.KOLOROWY);
		else this.wkladKol=new Tusz(Kolor.KOLOROWY);
	}

	public DrukarkaKolorowa() {
		super();
	}
	
	public boolean drukujKolor(Dokument dok) {
		if (!super.drukuj(dok)) return false;
		return wkladKol.eksploatuj(dok.getLiczbaStron());
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nwk�ad kolorowy: "+wkladKol;
	}

	
	
	public void setWkladKol(WkladDrukujacy wklad) {
		if (this.wkladKol instanceof Toner && wklad instanceof Toner
				|| this.wkladKol instanceof Tusz && wklad instanceof Tusz )
			this.wkladKol=wklad;
		else 
			InterfaceHelpers.print("Blad: nie myl tonera z tuszem");
	}
}
