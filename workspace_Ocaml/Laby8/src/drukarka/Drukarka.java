package drukarka;

import materialy_eksploatacyjne.*;
import user_interface.InterfaceHelpers;
import zrodla.Dokument;

public class Drukarka {

	protected BlokPapieru papier;
	protected WkladDrukujacy wkladCzar;
	protected String port = "USB";

	public Drukarka() {
		wkladCzar = null;
		papier = new BlokPapieru(0);
	}

	public Drukarka(boolean czyLaserowa) {
		if (czyLaserowa) 
			wkladCzar=new Toner();
		else 
			wkladCzar=new Tusz();
		papier=new BlokPapieru(0);
	}
	
	public Drukarka(BlokPapieru zasobnik, WkladDrukujacy wklad) {
		this.papier = zasobnik;
		if (wklad.getKolor()==Kolor.CZARNY)
			this.wkladCzar = wklad;
		else
			InterfaceHelpers.print("ERROR: niepoprawny wklad drukujacy");
	}


	public boolean drukuj(Dokument dok) {
		if (Math.random()<0.2) {
			InterfaceHelpers.print("ERROR: nieznany blad");
			return false;
		}
		if (!papier.zuzyjKartki(dok.getLiczbaStron())) return false;
		if (wkladCzar!=null)
			 return wkladCzar.eksploatuj(dok.getLiczbaStron());
		else
			InterfaceHelpers.print("ERROR: brak tonera");
		return false;
		
	}

	public void dodajPapier(int ile) {
		this.papier.dodajKartki(ile);
	}

	public void setWkladCzar(WkladDrukujacy wkladCzar) {
		if (this.wkladCzar instanceof Toner && wkladCzar instanceof Toner
				|| this.wkladCzar instanceof Tusz && wkladCzar instanceof Tusz )
			this.wkladCzar=wkladCzar;
		else 
			InterfaceHelpers.print("Blad: nie myl tonera z tuszem");
	}
	
	public boolean czyLaserowa() {
		return (wkladCzar instanceof Toner);
	}
	
	@Override
	public String toString() {
		return "Drukarka - stan: \npapier: "+papier+" \nport: "+port+"\nwk�ad drukujacy: "+wkladCzar;
	}

	

}
