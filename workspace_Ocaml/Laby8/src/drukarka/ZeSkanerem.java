package drukarka;

import zrodla.Dokument;

public interface ZeSkanerem {

	public default Dokument skanuj(int ileStron, String nazwa) {
		return new Dokument(ileStron, nazwa);
	};
}
