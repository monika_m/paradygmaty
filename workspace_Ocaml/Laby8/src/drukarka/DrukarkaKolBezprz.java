package drukarka;

import materialy_eksploatacyjne.BlokPapieru;
import materialy_eksploatacyjne.WkladDrukujacy;

public class DrukarkaKolBezprz extends DrukarkaKolorowa implements Bezprzewodowa {

	public DrukarkaKolBezprz(BlokPapieru zasobnik, WkladDrukujacy wklad, WkladDrukujacy wkladKol) {
		super(zasobnik, wklad, wkladKol);
	}


	public DrukarkaKolBezprz(boolean czyLaserowa) {
		super(czyLaserowa);
		
	}

	public DrukarkaKolBezprz() {
		
	}

	@Override
	public boolean skonfigurujPolaczenie(String nazwa_sieci, String haslo) {
		if (Math.random() < 0.5) {
			this.port = "Wifi: " + nazwa_sieci;
			return true;
		} else {
			return false;
		}

	}

}
