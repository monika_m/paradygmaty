package user_interface;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public abstract class InterfaceHelpers {

	public static void main(String args[]) {
		Menu menu=new Menu();
		menu.run();
	}

	static int getInt() { //gets Integer >0 from user
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int ret=-1;
		try {
			ret=Integer.parseInt(br.readLine());
			
		} catch (Exception e) {
			System.out.println("podaj poprawn� warto��");
			ret = getInt();
		}
		if (ret<0) {
			System.out.println("podaj wartosc wieksza od 0");
			ret=getInt();
		}
		return ret;
	}
	
	static String getString() {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		String ret="";
		try {
			ret=br.readLine();
		} catch (Exception e) {
			ret=getString();
		}
		return ret;
	}
	
	static boolean czyToner() {
		System.out.println("Czy drukarka ma by� laserowa?\n"
				+"0-nie, na tusz\n1-tak, na toner");
		int ret=getInt();
		if (ret==0) 
			return false;
		else if (ret==1)
			return true;
		else return czyToner();
	}
	public static void print(String text) {
		System.out.println(text);
	}
}
