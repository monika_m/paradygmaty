package user_interface;


import drukarka.*;
import materialy_eksploatacyjne.Kolor;
import materialy_eksploatacyjne.Toner;
import materialy_eksploatacyjne.Tusz;
import zrodla.Dokument;

class Menu {

	Drukarka druk;
	public Menu() {
		 druk=kupDrukarke();
	}

	public void run() {
		System.out.println("Witaj!");
		menu();
		System.out.println("Do widzenia");
	}
	
	private Drukarka kupDrukarke() {
		
		System.out.println("Kup nowe urz�dzenie\nWybierz typ: ");
		System.out.println("1-Drukarka czarna\n"+
				"2-Drukarka kolorowa\n"+
				"3-Drukarka czarna bezprzewodowa\n"+
				"4-Drukarka kolorowa bezprzewodowa\n"+
				"5-Urzadzenie wielofuncyjne ");
		int ret=InterfaceHelpers.getInt();
		if (ret==1) 
			return new Drukarka(InterfaceHelpers.czyToner());
		else if (ret==2)
			return new DrukarkaKolorowa(InterfaceHelpers.czyToner());
		else if (ret==3)
			return new DrukarkaBezprzewodowa(InterfaceHelpers.czyToner());
		else if (ret==4)
			return new DrukarkaKolBezprz(InterfaceHelpers.czyToner());
		else if (ret==5)
			return new UrzadzenieWielofunkcyjne(InterfaceHelpers.czyToner());
		else return kupDrukarke();
			
	}
	private void menu() {
		System.out.println("Wybierz, co chcesz zrobi�\n"
				+"0 - wy��cz\n"
				+ "1 - Drukuj\n2 - Do�aduj jeden z zasobnik�w\n"
				+"3 - Wy�wietl stan drukarki");
		if (druk instanceof Bezprzewodowa && druk instanceof ZeSkanerem) {
			menuBezprzewSkan();
		}
		else if (druk instanceof Bezprzewodowa)
			menuBezprzewodowa();
		else if (druk instanceof ZeSkanerem)
			menuZeSkanerem();
		else menuZwyk�a();
	}

	private void menuZwyk�a() {
		int resp = InterfaceHelpers.getInt();
		if (resp==0) { 
			return;
		} else if (resp==1) {
			drukuj();
		} else if (resp==2) {
			laduj();
		} else if (resp==3) {
			System.out.println(druk.toString()+"\n");
		} else {
			System.out.println("nie ma takiej opcji");
			
		}
		menu();
	}

	private void menuZeSkanerem() {
		System.out.println("4 - Skanuj\n");
		int resp = InterfaceHelpers.getInt();
		if (resp==0) {
			return;
		} else if (resp==1) {
			drukuj();
		} else if (resp==2) {
			laduj();
		} else if (resp==3) {
			System.out.println(druk.toString()+"\n");
		} else if (resp==4) {
			skanowanie();
		}
		else {
			System.out.println("nie ma takiej opcji");	
		}
		menu();
	}

	
	private void menuBezprzewSkan() {
		System.out.println("4 - Skanuj");
		System.out.println("5 - Skonfiguruj polaczenie bezprzewodowe");
		int resp = InterfaceHelpers.getInt();
		if (resp==0) {
			return;
		} else if (resp==1) {
			drukuj();
		} else if (resp==2) {
			laduj();
		} else if (resp==3) {
			System.out.println(druk.toString()+"\n");
		} else if (resp==4) {
			skanowanie();
		}else if (resp==5) {
			skonfigurujPolaczenie();
		}
		else {
			System.out.println("nie ma takiej opcji");	
		}
		menu();
	}

	private void menuBezprzewodowa() {
		System.out.println("4 - Skonfiguruj polaczenie bezprzewodowe\n");
		int resp = InterfaceHelpers.getInt();
		if (resp==0) {
			return;
		} else if (resp==1) {
			drukuj();
		} else if (resp==2) {
			laduj();
		} else if (resp==3) {
			System.out.println(druk.toString()+"\n");
		} else if (resp==4) {
			skonfigurujPolaczenie();
		}else {
			System.out.println("nie ma takiej opcji");
			
		}
		menu();
	}
	
	private void drukuj() {
		boolean sukces;
		System.out.println("Podaj nazw� dokumentu: ");
		String nazwa=InterfaceHelpers.getString();
		System.out.println("Podaj ilosc stron do druku: ");
		int str = InterfaceHelpers.getInt();
		Dokument dok=new Dokument(str, nazwa);
		if (druk instanceof DrukarkaKolorowa) {
			System.out.println("Druk kolorowy?\n0-nie\n1-tak");
			int ret=InterfaceHelpers.getInt();
			if (ret==1) {
				sukces=((DrukarkaKolorowa) druk).drukujKolor(dok);
			} else
				sukces=druk.drukuj(dok);
		} else
			sukces=druk.drukuj(dok);
		if (sukces) System.out.println("Wydrukowano "+dok.getLiczbaStron()+" stron dokumentu "+dok.getNazwa());
	}

	private void laduj() {
		System.out.println("Co chcesz uzupe�ni�?\n"
				+"1-do�� kartek");
		if (druk.czyLaserowa())
			System.out.println("2-nowy toner");
		else
			System.out.println("2-nowy tusz");
		int ret =InterfaceHelpers.getInt();
		
		if (ret==1) {
			System.out.println("Podaj liczb� kartek");
			int ile=InterfaceHelpers.getInt();
			druk.dodajPapier(ile);
		} else if (ret==2) {
			System.out.println("Podaj pojemnosc (liczbe stron): ");
			int str=InterfaceHelpers.getInt();
			if (druk instanceof DrukarkaKolorowa) {
				System.out.println("Czarny(0) czy kolorowy(1)?");
				int kol=InterfaceHelpers.getInt();
				if (kol==0) {
					if (druk.czyLaserowa())
						druk.setWkladCzar(new Toner(str,Kolor.CZARNY));
					else 
						druk.setWkladCzar(new Tusz(str, Kolor.CZARNY));
				} else if (kol==1) {
					if (druk.czyLaserowa())
						druk.setWkladCzar(new Toner(str,Kolor.KOLOROWY));
					else 
						druk.setWkladCzar(new Tusz(str, Kolor.KOLOROWY));
				} else {//kol!=0,!=1
					System.out.println("B��d");
				}
			}
		} else 
			System.out.println("B�ad, nie ma takiej opcji");
		
	}
	
	private void skonfigurujPolaczenie() {
		System.out.println("Skonfiguruj polaczenie bezprzewodowe.\nPodaj nazwe sieci Wifi: ");
		String nazwa_sieci=InterfaceHelpers.getString();
		System.out.println("Podaj has�o: ");
		String haslo=InterfaceHelpers.getString();
		if(((Bezprzewodowa)druk).skonfigurujPolaczenie(nazwa_sieci, haslo)) 
			System.out.println("Podlaczono do sieci");
		else 
			System.out.println("B��d. Nie uda�o si� pod��czy� do sieci.");
	}

	private Dokument skanuj() {
		System.out.println("Ile stron chcesz zeskanowa�?");
		int str=InterfaceHelpers.getInt();
		System.out.println("Podaj nazw� nowego dokumentu");
		String nazwa=InterfaceHelpers.getString();
		return ((ZeSkanerem)druk).skanuj((int)1.1*str, nazwa);
	}

	private void skanowanie() {
		Dokument doc =skanuj();
		System.out.println("Utworzono "+doc.getNazwa());
		System.out.println("Ile razy chcesz go wydrukowa�?");
		int ile=InterfaceHelpers.getInt();
		for (int i=0; i<ile; i++) {
			if(!druk.drukuj(doc))
				i=ile;
			else 
				System.out.println("Wydrukowano "+doc.getNazwa());
		}
	}


}
