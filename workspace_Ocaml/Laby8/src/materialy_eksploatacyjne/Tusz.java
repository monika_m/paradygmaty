package materialy_eksploatacyjne;

public class Tusz extends WkladDrukujacy {

	public Tusz() {
		super(100, Kolor.CZARNY);
	}

	public Tusz(int Strony, Kolor Kolor) {
		super(Strony, Kolor);
	}

	@Override
	public String toString() {
		return "Tusz "+getKolor()+" zostalo na "+iloscStron+" stron";
	}

	public Tusz(Kolor Kolor) {
		super(Kolor);
		// TODO Auto-generated constructor stub
	}
}
