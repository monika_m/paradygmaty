package materialy_eksploatacyjne;

public class Toner extends WkladDrukujacy {

	public Toner() {
		super(1000, Kolor.CZARNY);
	}

	public Toner(int Strony) {
		super(Strony, Kolor.CZARNY);
	}
	
	public Toner(Kolor Kolor) {
		super(Kolor);
		// TODO Auto-generated constructor stub
	}

	public Toner(int Strony, Kolor Kolor) {
		super(Strony, Kolor);
	}

	

	@Override
	public String toString() {
		return "Toner "+getKolor()+" zostalo na "+iloscStron+" stron";
	}
}
