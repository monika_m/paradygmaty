package materialy_eksploatacyjne;
import user_interface.InterfaceHelpers;

public class BlokPapieru {

	private int iloscKartek;
	
	public BlokPapieru() {
		this.iloscKartek=0;
	}

	public BlokPapieru(int IloscKartek) {
		this.iloscKartek = IloscKartek;
	}
	
	public void dodajKartki(int IloscKartek) {
		this.iloscKartek+=IloscKartek;
	}
	
	public boolean zuzyjKartki(int IloscKartek) {
		this.iloscKartek-=IloscKartek;
		if (this.iloscKartek<0) {
			InterfaceHelpers.print("ERROR! Zabrak�o kartek. Niepowodzenie drukowania");
			this.iloscKartek=0;
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return iloscKartek+" kartek ";
	}
}
