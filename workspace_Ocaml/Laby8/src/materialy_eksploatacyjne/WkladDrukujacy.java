package materialy_eksploatacyjne;

import user_interface.InterfaceHelpers;

public abstract class WkladDrukujacy {

	protected int iloscStron;
	protected final Kolor kolor;
	

	public WkladDrukujacy() {
		this.iloscStron = 0;
		this.kolor = Kolor.CZARNY;
	}

	public WkladDrukujacy(Kolor Kolor) {
		this.iloscStron=100;
		this.kolor=Kolor;
	}
	public WkladDrukujacy(int Strony, Kolor Kolor) {
		this.iloscStron = Strony;
		this.kolor = Kolor;
	}

	public Kolor getKolor() {
		return kolor;
	}
	public boolean eksploatuj(int IloscStron) {
		if (this.iloscStron >= IloscStron) {
			this.iloscStron -= IloscStron;
			return true;
		} else {
			this.iloscStron=0;
			InterfaceHelpers.print("ERROR! Zabrak�o tuszu");
		}
		return false;
	}

}
