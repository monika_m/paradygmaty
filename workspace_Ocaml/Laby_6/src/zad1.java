public class zad1 {
	//static <A extends Comparable<A>> void insert (A el, A[] tab){
	static int[] insert (int el, int[] tab){
		//A[] newTab= (A[])(new Object[tab.length+1]);// java.lang.ClassCastException: [Ljava.lang.Object; cannot be cast to [Ljava.lang.Comparable;
		int[] newTab=new int[tab.length+1];
		int j=0;
		for (int i=0; i<tab.length; i++) {
			//if (tab[i].compareTo(el)<=0) {
			if (tab[i]<=el) {
				newTab[j]=tab[i];
			} else {
				newTab[j]=el;
				while(i<tab.length) {	
					newTab[++j]=tab[i];
					i++;
				}
			}
			j++;
		}//for
		if (j==tab.length)
			newTab[j]=el;
		return newTab;
	};
	
	public static void main(String[] args) {
		int[] tab = {1,2,3};
		System.out.println(view(insert (0, tab)));
		System.out.println(view(insert (2, tab)));
		System.out.println(view(insert (4, tab)));
	}
	//static <A> String view(A[] tab) {
	static  String view(int[] tab) {
		String ret="[";
		for (int i =0; i<tab.length; i++) {
			ret+=tab[i]+",";
		}
		ret+="]";
		return ret;
	}
}
