let root3 a =
	let rec root3check x =
		if abs_float((x*.x*.x)-. a) <= 1e-015 *. abs_float(a) then x
		else root3check(x +. (a /.(x*.x) -. x) /. 3.)
	in root3check(if a <= 1. then a else a /. 3.);;


root3 1.0;;
root3 (-1.0);;
root3 27.;;
root3 2.;;