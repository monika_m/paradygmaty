let a = [-2;  -1;  0;  1;  2];;

let [_;_;x;_;_] = a;;

let b =  [ (1,2); (0,1) ];;
let [_;(y,_)] = b;;

let [_;_;x;_;_] =[-2;  -1;  0;  1;  2];;	(*5 elementowe*)
let _::_::x::_::_=[-2;  -1;  0;  1;  2];; (*co najmniej 4 elementowe*)
let _::_::x::_=[-2;  -1;  0;  1;  2];; (*co najmniej 3 el*)
let _::_::x::_::_::_=[-2;  -1;  0;  1;  2];; (*co najmniej 5 el*)