let rec initSegment (xs1, xs2) =
	if (xs1=[]) then true else
		if (xs2=[]) then false else
	match (xs1, xs2) with
	| (h1::t1, h2::t2) -> if (h1=h2) then initSegment(t1, t2) else false;;
