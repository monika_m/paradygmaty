let rec replaceNth(xs, n, x)=
	match (xs, n) with
	| ([],_) -> []
	| (h::t, 0) -> x::t
	| (h::t, _) -> h::replaceNth(t, n-1, x);;






let rec replaceNth1 (xs, n, x)=
	if (n=0) then x::List.tl xs
	else List.hd xs :: replaceNth1(List.tl xs, n-1, x)