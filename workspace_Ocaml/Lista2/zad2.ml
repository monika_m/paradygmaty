let rec fib1 n = 
	if (n=0) then 0 else if (n=1) then 1 else fib1 (n-1)+fib1(n-2);;

let fib2 n =
	let rec fibb (n, f1, f2) =
		if n=0 then f1 
		else if n=1 then f2 
		else fibb(n-1, f2, f1+f2)
	in fibb(n,0,1);; 

fib1 0;;
fib1 1;;
fib1 2;;
fib1 3;;
fib1 42;;
fib2 0;;
fib2 1;;
fib2 2;;
fib2 3;;
fib2 42;;
		
		