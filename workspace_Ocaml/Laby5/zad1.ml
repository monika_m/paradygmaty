type 'a nlist = Koniec | Element of 'a * ('a nlist);; 
type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);; 

(*let lhd = function LNil -> failwith "lhd" | LCons (x, _) -> x ;;
let ltl = function LNil -> failwith "ltl" | LCons (_, xf) -> xf() ;; 
let rec lfrom k = LCons (k, function () -> lfrom (k+1));;*)
let rec ltake = function (0, _) -> [] | (_, LKoniec) -> [] | (n, LElement(x,xf)) -> x::ltake(n-1, xf()) ;;
let rec toLazyList = function [] -> LKoniec | h::t -> LElement(h, function () -> toLazyList t);;
let rec toList = function
    | LKoniec -> []
    | LElement(x, xs) -> x :: toList (xs());;


let rec podziel nlist =
	match nlist with
	| Koniec -> (Koniec, Koniec)
	| Element (x, Koniec) -> (nlist, Koniec)
	| Element (x, Element(y, t)) ->  
		let (even, odd) = podziel t in (Element(x, even), Element(y,odd));;
	
podziel Koniec;;
podziel (Element(1, Element(2, Element(3,Element(4,Koniec)))));;
podziel (Element('a', Element('b', Element('c', Koniec))));;

let rec lpodziel llist =
	match llist with
	| LKoniec -> (LKoniec, LKoniec)
	| LElement (x, t1)-> 
		match t1() with
		| LKoniec -> (llist, LKoniec)
		| LElement(y, t) ->  
		let (even, odd) = lpodziel (t()) 
			in (LElement(x, function () -> even), LElement(y,function () ->odd));;
let read ll=(toList(fst ll),toList(snd ll));;

let a = lpodziel LKoniec;;
let llist1= toLazyList [1;2;3;4];;
let b = lpodziel llist1;;
read b;;
let llist2 = toLazyList ['a';'b';'c'];;
let c = lpodziel llist2;;
read c;;






















(*let podziel nlist =
	let rec getEverySec list=
		match list with
		| Koniec -> Koniec
		| Element(x, t) -> match t with
									| Koniec -> Element (x, Koniec)
									| Element(x2,t2) -> Element(x, getEverySec t2) in
	match nlist with
	| Koniec -> (Koniec,Koniec)
	| Element(x, t) as listt -> (getEverySec listt, getEverySec t);;

let podziel llist =
	let rec getEverySec list=
		match list with
		| LKoniec -> LKoniec
		| LElement(x, t) -> match t() with
									| LKoniec -> LElement (x,  function () -> LKoniec)
									| LElement(x2,t2) -> LElement(x,  function () -> getEverySec (t2())) in
	match llist with
	| LKoniec -> ([],[])(*(LKoniec, LKoniec)*)
	| LElement(x, t) as listt -> (ltake(2,getEverySec listt), ltake(2,getEverySec (t())));;

let llist= toLazyList [1;2;3;4];;
let a = podziel llist;;
let read a = (ltake (2,a fst), ltake(2, a snd));;
read a;;*)