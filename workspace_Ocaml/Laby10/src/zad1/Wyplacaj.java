package zad1;

class Wyplacaj extends Thread {

	private StanKontaBankowego konto;

	Wyplacaj(StanKontaBankowego stan) {
		super();
		this.konto = stan;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			int stanKonta = konto.getStanKonta();
			int sleepTime = (int) (Math.random() * 2000);
			try {
				sleep(sleepTime);
				konto.setStanKonta(stanKonta - 100);
				System.out.println(stanKonta - 100);
				sleepTime = (int) (Math.random() * 2000 + 2000);
				sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}
}
