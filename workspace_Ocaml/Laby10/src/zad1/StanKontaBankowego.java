package zad1;

class StanKontaBankowego {

	private int stanKonta = 1000;

	int getStanKonta() {
		return stanKonta;
	}

	void setStanKonta(int stanKonta) {
		this.stanKonta = stanKonta;
	}

	public static void main(String[] args) {
		StanKontaBankowego konto = new StanKontaBankowego();
		Wplacaj wplata = new Wplacaj(konto);
		Wyplacaj wyplata = new Wyplacaj(konto);
		wplata.start();
		wyplata.start();

		/*
		 * 900 1100 1000 1100 1000 1100 1000 1100 900 800 1000 1100 900 1000 800 700 800
		 * 700 800 900 
		 * koncowo:900, powinno byc: 1000 
		 * 900 1100 1000 1100 1000 1200 1100 1200 1100 1200 1100 1200 1000 900 1100 1000 
		 * 1200 1100 1300 
		 * 1400
		 */

	}

}
