package zad2;

class Wyplacaj extends Thread {

	private StanKontaBankowego konto;

	Wyplacaj(StanKontaBankowego stan) {
		super();
		this.konto = stan;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			konto.flagaWyplacaj = true;

			while (konto.flagaWplacaj) {
				if (konto.pierwszenstwo != 1) {
					konto.flagaWyplacaj = false;
					while (konto.pierwszenstwo != 1) {
						// System.out.println("wyplacaj czeka");
					}
					konto.flagaWyplacaj = true;
				}

			}

			int stanKonta = konto.getStanKonta();
			int sleepTime = (int) (Math.random() * 2000);
			try {
				sleep(sleepTime);
				konto.setStanKonta(stanKonta - 100);
				System.out.println(stanKonta - 100);

			} catch (InterruptedException e) {
			}

			konto.pierwszenstwo = 0;
			konto.flagaWyplacaj = false;
			sleepTime = (int) (Math.random() * 2000 + 2000);
			try {
				sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
