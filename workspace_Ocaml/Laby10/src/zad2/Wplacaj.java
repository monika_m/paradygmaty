package zad2;

class Wplacaj extends Thread {

	private StanKontaBankowego konto;

	Wplacaj(StanKontaBankowego stan) {
		super();
		this.konto = stan;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			konto.flagaWplacaj = true;

			while (konto.flagaWyplacaj) {
				if (konto.pierwszenstwo != 0) {
					konto.flagaWplacaj = false;
					while (konto.pierwszenstwo != 0) {
						// System.out.println("wplacaj czeka");
					}
					konto.flagaWplacaj = false;
				}

			}

			int stanKonta = konto.getStanKonta();
			int sleepTime = (int) (Math.random() * 2000);
			try {
				sleep(sleepTime);
				konto.setStanKonta(stanKonta + 100);
				System.out.println(stanKonta + 100);

			} catch (InterruptedException e) {
			}

			konto.pierwszenstwo = 1;
			konto.flagaWplacaj = false;
			sleepTime = (int) (Math.random() * 2000 + 2000);
			try {
				sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
