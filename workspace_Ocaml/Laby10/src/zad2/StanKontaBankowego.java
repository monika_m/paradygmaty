package zad2;

class StanKontaBankowego {

	private int stanKonta = 1000;

	volatile boolean flagaWplacaj=false;
	volatile boolean flagaWyplacaj=false;
	volatile int pierwszenstwo=0;
	
	int getStanKonta() {
		return stanKonta;
	}

	void setStanKonta(int stanKonta) {
		this.stanKonta = stanKonta;
	}

	public static void main(String[] args) {
		StanKontaBankowego konto = new StanKontaBankowego();
		Wplacaj wplata = new Wplacaj(konto);
		Wyplacaj wyplata = new Wyplacaj(konto);
		wplata.start();
		wyplata.start();


	}

}
