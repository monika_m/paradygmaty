package zad3;

import java.util.concurrent.Semaphore;

class Filozof extends Thread {

	private int id;
	Paleczka paleczki[];
	Semaphore straznik;

	
	
	public Filozof(int id, Paleczka[] paleczki, Semaphore straznik) {
		super();
		this.id = id;
		this.paleczki = paleczki;
		this.straznik = straznik;
	}

	@Override
	public void run() {

		for (int i = 0; i < 5; i++) {
			int czasMyslenia = (int) (Math.random() * 1000);
			//System.out.println("Filozof " + id + " my�li");
			try {
				sleep(czasMyslenia);
			} catch (InterruptedException e) {
			}

			try {
				eat();
			} catch (InterruptedException e) {
			}
		}
	}

	private void eat() throws InterruptedException {
		straznik.acquire();
		paleczki[id].wez();
		paleczki[(id+1)%paleczki.length].wez();
		int czasJedzenia=(int) (Math.random()*1000);
		System.out.println("Filozof "+id+" je");
		sleep(czasJedzenia);
		paleczki[id].odloz();
		paleczki[(id+1)%paleczki.length].odloz();
		System.out.println("Filozof "+id+" skonczyl jesc");
		straznik.release();
	}

}
