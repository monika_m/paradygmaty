package zad3;

import java.util.concurrent.Semaphore;

class Paleczka {

	private Semaphore wolna=new Semaphore(1);
	
	void wez() throws InterruptedException {
		wolna.acquire();
	}
	
	void odloz() {
		wolna.release();
	}
}
