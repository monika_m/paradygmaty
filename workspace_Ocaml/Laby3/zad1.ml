let rec filterList x listl=
	let rec check x list = 
		match list with 
		| [] -> false 
		| h::t -> if (h=x) then true else check x t in
	match listl with
	| [] -> []
	| h::t -> if ((check x h)=false) then h::filterList x t else filterList x t;;

filterList 2 [[2;3];[1;3];[];[3;3;2]];;
filterList 'a' [['a';'b']; ['b';'b';'b'];['c';'2';'a'];[]];;