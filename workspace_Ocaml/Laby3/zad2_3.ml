let rec numbers bin=
	if (bin=[]) then 0
	else List.hd bin * int_of_float(2.**float_of_int(List.length bin -1)) + numbers (List.tl bin);;

numbers [1];;
numbers [0;1];;
numbers [1;1];;
numbers [1;0;1];;
numbers [1;0;0;1;0;1];;