type 'a graph= Graph of ('a -> 'a list);;

 let g = Graph (function 
	| 0 -> [3] 
	| 1 -> [0;2;4] 
	| 2 -> [1] 
	| 3 -> [] 
	| 4 -> [0;2] 
	| n -> failwith ("Graph g: node "^string_of_int n^" doesn't exist") );;

let depthSearch (Graph g) el=
	let rec search els visited = 
		match els with
		| [] -> []
		| h::t -> if (List.mem h visited) then search t visited
							else h::(search (g h@t) (h::visited))
	in search [el] [];;
	
depthSearch g 4;;
	
