type 'a tree = Empty | Node of 'a * 'a tree *'a tree;;

let  breadthSearch tree = 
	let rec search visited node=
		match node with 
		| Empty -> visited
		| Node(v, left, right) -> if List.mem v visited then search (search visited left ) right 
															else search (search (visited@[v]) left ) right 
	in search [] tree;;

let tt = Node(1, Node(2, Node(4, Empty, Empty ), Empty ), Node(3, Node(5, Empty, Node(6, Empty, Empty ) ), Empty ) );;  

breadthSearch tt;;

(*zad4*)
let  intPath tree = 
	let rec search node path=
		match node with 
		| Empty -> 0
		| Node(_, left, right) -> path+ search left (path+1) + search right (path+1)
	in search tree 0;;

intPath tt;;

let extPath tree = 
	let rec search node path=
		match node with 
		| Empty -> path
		| Node(_, left, right) -> search left (path+1) + search right (path+1)
	in search tree 0;;

extPath tt;;

(*let breadthBT tree =
	let rec helper = function
			[] -> []
		| [] :: tail -> helper tail
		| Node(value, leftSubtree, rightSubtree) :: tail -> value :: helper ([leftSubtree ::rightSubtree :: tail])
	in helper [tree];;*)

(*let rec list2bst l = 
	let rec insert2bst = function (k, Node (r,lt,rt)) -> 
		if k<r then Node(r,insert2bst(k,lt),rt) 
		else if k>r then Node(r,lt,insert2bst(k,rt)) 
		else failwith "duplicated key" 
	| (k, Empty) -> Node(k,Empty,Empty) 
 in
	 match l with 
	| h::t ->  insert2bst(h, list2bst t) 
	| [] -> Empty;;*)