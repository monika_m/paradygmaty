 type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;

let lfib n=
	let rec fib (n, f1,f2)=
		if (n==0) then LNil
		else if (n==1) then LCons(f1, function() -> LNil) 
		else LCons(f1, function() -> fib(n-1, f2, f1+f2))
	in fib(n,0,1);;

let rec toList = function
    | LNil -> []
    | LCons(x, xs) -> x :: toList (xs());;

let a= lfib 5;;
toList a;;