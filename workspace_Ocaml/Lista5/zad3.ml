 type 'a lBT = LEmpty  |  LNode of  'a * (unit ->'a lBT) * (unit -> 'a lBT);;
  type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;
 
let breadthSearch startNode =
  	let rec search (visited, queue) =
  		 match queue with
  		
  		| h::t -> if (List.mem h visited) then search (visited, t)
  		  			else	 match h with
  				| LNode(elem,left, right)-> LCons( elem, function()->search ((h::visited), (List.rev (right()::left()::(List.rev t))) ))
  				| LEmpty -> search ((visited), (t ) )
 			| [] -> LNil (*wykona si� co najwy�ej raz*)
		in search ([], [startNode]);;
  	
 

let a=breadthSearch(LNode (1, 
 						(function() -> LNode(2, 
 									(function() -> LNode(3, 
 												(function() -> LEmpty), 
 												(function() -> LEmpty))), 
 									(function() -> LNode(4, 
 												(function() -> LEmpty), 
 												(function() -> LEmpty)))
 									)),
 						(function() -> LEmpty)
 						)
 		);;

 		
let rec toList = function
    | LNil -> []
    | LCons(x, xs) -> x :: toList (xs());;
    
 toList a;;
 
 
 (* b *)
 let rec lTree n=
 	LNode(n, (function() -> lTree (2*n)), (function() -> lTree (2*n+1))) ;;