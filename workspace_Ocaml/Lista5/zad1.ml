 type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;

let rec lpowiel n nlist=
	let powiel2 el t =
		let rec powiel3 el count =
			if (count=0) then lpowiel n t
			else LCons(el, function() ->powiel3 el (count-1))
		in powiel3 el n
	in
	match nlist with
	| LNil -> LNil
	| LCons(h, t) -> powiel2 h (t());;

let rec toLazyList = function [] -> LNil | h::t -> LCons(h, function () -> toLazyList t);;
let rec toList = function
    | LNil -> []
    | LCons(x, xs) -> x :: toList (xs());;

let a=lpowiel (toLazyList[2]) 3;;
toList a;;
let b= lpowiel (toLazyList[0]) 4;;
toList b;;
let c=lpowiel LNil 2;;
toList c;;
let d=lpowiel (toLazyList[1;2;5;1]) 2;;
toList d;;

let lrepeat k llist=
let rec helper(n,list)=
	match (n,list) with
	| (0,LCons(_,tail))-> helper(k,tail())
	| (_,LCons(head,_))-> LCons(head, function()->helper(n-1,list))	
	| (_,LNil)->LNil
in helper(k, llist);;
let a=lrepeat 3 (toLazyList[2;3;4]);;
toList a;;