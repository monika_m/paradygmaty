(*type 'a bt = Empty | Node of 'a * 'a bt * 'a bt;;*)
type int_tree = Empty | Node of int * int_tree * int_tree;;

let rec list2bst l = 
	let rec insert2bst = function 
		| (k, Node (r,lt,rt)) -> 
			if k<r then Node(r,insert2bst(k,lt),rt) 
			else if k>r then Node(r,lt,insert2bst(k,rt)) 
			else failwith "duplicated key" 
	| (k, Empty) -> Node(k,Empty,Empty) 
 in
	 match l with 
	| h::t ->  insert2bst(h, list2bst t) 
	| [] -> Empty;;

let rec contains bst x =
	match bst with
		| Empty -> false
		| Node (v, l, r) ->
			if (v=x) then true 
			else if (x<v) then contains l x
			else contains r x;;

let rec sum_tree bst = 
	match bst with
		| Empty -> 0
		| Node (v, l, r) -> v + sum_tree l + sum_tree r;;

let rec prod_tree bst = 
	match bst with
		| Empty -> 0
		| Node (v, l, r) -> 
			if (l=Empty && r==Empty) then v
			else if (l=Empty) then v * prod_tree r
			else if (r=Empty) then v * prod_tree l
			else v * prod_tree l * prod_tree r;;
	

let tree = list2bst [13;7;4;14;6;1;10;3;8];;
contains tree 13;;
contains tree 0;;
contains Empty 2;;
sum_tree tree;;
sum_tree Empty;;
prod_tree tree;;
prod_tree Empty;;