package zad4;

import java.util.concurrent.Semaphore;

class Main {

	public static void main(String[] args) {

		Semaphore odzwierny = new Semaphore(4, true);
		Paleczka paleczki[] = new Paleczka[5];
		for (int i=0; i<paleczki.length; i++) {
			paleczki[i]=new Paleczka();
		}

		Filozof filozofy[] = new Filozof[5];
		for (int i = 0; i < filozofy.length; i++) {
			filozofy[i] = new Filozof(i, paleczki, odzwierny);
		}

		for (int i = 0; i < filozofy.length; i++) {
			filozofy[i].start();
		}

	}

}
