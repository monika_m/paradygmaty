package zad2a;

class IntCell {
	private int n = 0;
	private boolean busy = false;

	public synchronized int getN() {
		/*
		 * Metody te s� zaimplementowane w klasie Object. Wolno je jednak wywo�ywa�
		 * tylko z kodu synchronizowanego, zajmuj�c obiekt, z kt�rego s� wywo�ywane
		 */
		while (busy) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		busy=true;
		notifyAll();
		return n;

	}

	public synchronized void setN(int n) {
		while (!busy) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		this.n = n;
		busy=false;
		notifyAll();
	}
}

class Count extends Thread {
	private static IntCell n = new IntCell();

	@Override
	public void run() {
		int temp;
		for (int i = 0; i < 200000; i++) {
			temp = n.getN();
			n.setN(temp + 1);
		}
	}

	public static void main(String[] args) {
		Count p = new Count();
		Count q = new Count();
		p.start();
		q.start();
		try {
			p.join();
			q.join();
		} catch (InterruptedException e) {
		}
		System.out.println("The value of n is " + n.getN());
	}
}
