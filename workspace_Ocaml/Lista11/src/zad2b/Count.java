package zad2b;

import java.util.concurrent.Semaphore;

class IntCell {
	private int n = 0;
	private Semaphore semafor= new Semaphore(1);//static �eby wszystkie wyst�pienia klasy mia�y ten sam (tu nie ma, bo IntCell jest statyczny w Councie)

	public void zadaj() {
		try {
			semafor.acquire();
		} catch (InterruptedException e) {}
	}
	
	public void zwolnij () {
		semafor.release();
	}
	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
}

class Count extends Thread {
	private static IntCell n = new IntCell();

	@Override
	public void run() {
		int temp;
		for (int i = 0; i < 200000; i++) {
			n.zadaj();
			temp = n.getN();
			n.setN(temp + 1);
			n.zwolnij();
		}
	}

	public static void main(String[] args) {
		Count p = new Count();
		Count q = new Count();
		p.start();
		q.start();
		try {
			p.join();
			q.join();
		} 
		catch (InterruptedException e) {
		}
		System.out.println("The value of n is " + n.getN());
	}
}
