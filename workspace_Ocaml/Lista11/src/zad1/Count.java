package zad1;

class IntCell {
	private int n = 0;

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
}

class Count extends Thread {
	private static IntCell n = new IntCell();

	@Override
	public void run() {
		int temp;
		for (int i = 0; i < 200000; i++) {
			temp = n.getN();
			n.setN(temp + 1);
		}
	}

	public static void main(String[] args) {
		Count p = new Count();
		Count q = new Count();
		p.start();
		q.start();
		try {
			p.join();
			q.join();
		} // without it 0
		catch (InterruptedException e) {
		}
		System.out.println("The value of n is " + n.getN());
	}
	// niedeterminizm <=przeplot + mutowalny stan
	//zmienna wsp�dzielona n
	/*
	 * Schemat fork/join zosta� zaprojektowany w celu podzia�u zadania, kt�re mo�na
	 * zr�wnolegli�, na mniejsze zadania, a po ich zako�czeniu wykorzystania
	 * otrzymanych wynik�w do wyprodukowania wyniku ko�cowego. Ten proces mo�na
	 * oczywi�cie powtarza� rekurencyjnie.
	 */

	//// 199798
	// 295140
	// 233517
	// 290068
	// 327870
	// 247177

}