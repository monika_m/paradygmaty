let rec replicate(x,n)=
	if (n>0) then [x]@replicate(x, n-1) else [];;
	
	replicate('a',5);;
	replicate(0,10);;