let palindrome xs=
	if(xs = (List.rev xs)) then true else false;;

palindrome [];;
palindrome [1];;
palindrome [1;2;1];;
palindrome ['a';'b';'b';'a'];;
palindrome [1;2];;
palindrome ['a';'d';'c'];;