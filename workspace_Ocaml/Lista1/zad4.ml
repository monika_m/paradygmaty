let rec sqrList xs=
	if (List.length xs > 0) then [List.hd xs * List.hd xs]@sqrList (List.tl xs) else [];;

sqrList([1;2;-5]);;