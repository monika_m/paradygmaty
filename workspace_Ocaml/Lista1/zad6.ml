let rec listLength(xs)=
	if(xs=[]) then 0 else (1 + listLength(List.tl xs));;

listLength([]);;
listLength([1;2]);;
listLength(['a';'b';'d']);;