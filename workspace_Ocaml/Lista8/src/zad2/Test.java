package zad2;

public class Test {  
	int zawarto�� = 0;  
	
	static void argNiemodyfikowalny(final Test zmienna) {   
		zmienna.zawarto�� = 1;   
		//zmienna = null;   //The final local variable zmienna cannot be assigned. It must be blank and not using a compound assignment
		}  
	
	static void argModyfikowalny(Test zmienna) {   
		zmienna.zawarto�� = 1;   
		zmienna = null;   
		}  
	/*Nie skompiluje si�, bo nast�puje pr�ba zmiany referencji zmiennej zmienna, co jest niemo�liwe
przy zastosowaniu modyfikatora final. Zmienn� z modyfikatorem final mo�na inicjowa� wy��cznie
jawnie (w kodzie) lub w konstruktorze, przy czym nigdzie nie mo�na zmienia� jej zawarto�ci. Dlatego
nale�y usun�� lini� kodu: zmienna = null, �eby si� kod skompilowa�.*/
	
	public static void main(String[] args) {   
		Test modyfikowalna = new Test();  
		final Test niemodyfikowalna = new Test();  
		// tutaj wstaw instrukcje  
		//a) 
		argNiemodyfikowalny(modyfikowalna);                 
		System.out.println(modyfikowalna.zawarto��); //1
		//b) 
		argNiemodyfikowalny(niemodyfikowalna);          
		System.out.println(niemodyfikowalna.zawarto��); //1
		//c) 
		argModyfikowalny(modyfikowalna);          
		System.out.println(modyfikowalna.zawarto��); //1
		//d) 
		argModyfikowalny(niemodyfikowalna);           
		System.out.println(niemodyfikowalna.zawarto��); //1
	}
	/*W ka�dym z podpunkt�w efekt
b�dzie taki sam, tzn. wy�wietlona zostanie 1, poniewa� odwo�anie si� do pola obiektu wskazywanego
przez referencj� spowoduje faktyczn� zmian� zawarto�ci tego pola, czego efekt b�dzie widoczny
poza metod� zmieniaj�c� to pole. Natomiast przypisanie do zmiennej �Test zmienna� referencji
pustej czyli null niczego nie zmienia, poniewa� parametry metod s� przekazywane przez warto�� czyli
s� traktowane jak zmienne lokalne zadeklarowane w tej metodzie, tzn. warto�ci zmiennych u�ytych
jako parametry s� kopiowane do zmiennych lokalnych metody.*/
}