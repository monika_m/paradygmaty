package zad1;

import java.util.ArrayList;
import java.util.List;

public class Queue<E> implements MyQueue<E> {

	private List<E> list;
	int f ,r;

	public Queue() {
		this(3);
	}

	public Queue(int size) {
		list = new ArrayList<E>(size + 1);
		for (int i = 0; i <= size; i++) {
			list.add(null);
		}
		r=f=0;
	}

	@Override
	public void enqueue(E el) throws FullException {
		if (list.size() != 0) {
			if ((r - f) % list.size() == 1)
				throw new FullException("Kolejka pe�na");
			else {
				list.set(r, el);
				//add zawsze dodaje, a set wk�ada do kolejki, psuje...
				r = (r + 1) % list.size();
			}
		} else
			throw new FullException("Kolejka zerowa");
	}

	@Override
	public void dequeue() {
		if (r != f) {
			list.set(f, null);
			f = (f + 1) % list.size();
		}
	}

	@Override
	public E first() throws EmptyException {
		if (r == f)
			throw new EmptyException("Pusta kolejka");
		else
			return list.get(f);
	}

	@Override
	public boolean isEmpty() {
		return (r == f);
	}

	@Override
	public boolean isFull() {
		if (list.size() != 0) {
			return (r + 1) % list.size() == f;//return ((f - r) % list.size() == 1);
		} else
			return true;
	}

	public static void main(String[] args) {
		Queue<Integer> my = new Queue<Integer>(1);
		System.out.println(my.isFull());
		try {
			my.enqueue(1);
		} catch (FullException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		System.out.println(my.isFull());
		
		try {
			System.out.println(my.first());
		} catch (EmptyException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		my.dequeue();
		try {
			System.out.println(my.first());
		} catch (EmptyException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		System.out.println(my.isEmpty());

	}
}
