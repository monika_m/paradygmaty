package zad1;

public class EmptyException extends Exception {

	public EmptyException() {
		super();
	}
	
	public EmptyException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
