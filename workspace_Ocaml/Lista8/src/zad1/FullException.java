package zad1;

public class FullException extends Exception {

	private static final long serialVersionUID = 1L;//The serializable class FullException does not declare a static final serialVersionUID field of type long

	public FullException() {
		super();
	}

	public FullException(String s) {
		super(s);
	}

}
