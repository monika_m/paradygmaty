import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String args[]) {
//Tablice w Javie s� kowariantne, tzn. je�li T1 <: T2, to T1[] <: T2[]
//Kolekcje s� inwariantne
		
		Komputer kompTab[] = new Laptop[1];
		Urzadzenie urzTab[] = new Urzadzenie[1];
		/*Laptop[] lapTab[]= new Urzadzenie[1];
		ArrayList<Komputer> kompList = new ArrayList<Laptop>();
		ArrayList<Komputer> kompList2= new ArrayList<Urzadzenie>();*/
		ArrayList<Laptop> lapList = new ArrayList<Laptop>(1);
		//lapList.add(new Urzadzenie());
		lapList.add(new Laptop());
		ArrayList<Urzadzenie> urzList = new ArrayList<Urzadzenie>(1);
		urzList.add(new Urzadzenie());
		urzList.add(new Laptop());
		changeHead(urzList, lapList);//wstawia laptopa do Listy Urzadzen
	}

	public static <T> void changeHead(List<? super T> toChange, List<? extends T> withNew) {
										//kontrawariantno��			//kowariantno��
		toChange.set(0, withNew.get(0));

	}
}
