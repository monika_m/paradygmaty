module type TREE=
  sig
    type tree = Leaf | Node of int * tree * tree
		type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);; 
    exception DuplicatedKey
    val create : unit -> tree
    val push : int * tree -> tree
		val remove : int *tree -> tree
		val find : int * tree -> tree
		val inOrder: tree ->int llist
		val preOrder: tree ->int llist
		val postOrder: tree ->int llist
		val toList: int llist-> int list
  end;;(*module type TREE*)

module BTree:TREE=
	struct
		type tree = Leaf | Node of int * tree * tree
		  exception DuplicatedKey 
		let create() = Leaf
		
		let rec push(v, tree) =   
			match tree with      
			| Leaf -> Node(v, Leaf, Leaf)    
			| Node(k,t1,t2) ->        
				if (v<k) then Node(k, push (v,t1), t2)          
				else if (v=k) then raise DuplicatedKey           
				else  Node(k, t1, push (v,t2))     

	 let rec deletemin tree =
		   match tree with         
			| Node(k,Leaf,t2) -> (k,t2)           (* This is the critical case. If the left subtree is empty, then the element at the current node is the min. *)       
			| Node(k,t1,t2) ->           
				let (value,l) = deletemin t1           
				in (value,Node(k,l,t2))       
			| Leaf -> failwith "Dictionary: implementation error"  
		
		
	let rec remove (key, tree) =     
		match tree with           
		| Leaf ->  Leaf  
		| Node(k,t1,t2) ->             
				if (key<k) then Node(k, remove(key, t1), t2)                
				else if (key>k) then(
					match (t1, t2) with     
					| (Leaf, t2) -> t2                       
					| (t1, Leaf)  -> t1                       
					| _  -> let (ki,t_right) = deletemin t2  
									 in Node(ki, t1,t_right)            
				)                   
				else Node(k, t1, remove (key,t2))   
				
	let rec find (key, tree)  =         
		match tree with           
		| Node(k,t1,t2) ->             
			if (key<k) then find (key,t1)               
			else if (key=k) then Node(k,t1,t2)              
			else find (key,t2)                       
		| Leaf ->Leaf      ;; 
 				
 	type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);; 
	
	let rec inOrder tree=
		let rec toLazyList = function 
			| [] -> LKoniec 
			| h::t -> LElement(h, function () -> toLazyList t) in 
		let rec order node=
			match node with
			| Leaf -> []
			| Node (v, l, r) -> ((order l)@[v]) @(order r) in
		toLazyList (order tree)
		
	let rec preOrder tree=
		let rec toLazyList = function 
			| [] -> LKoniec 
			| h::t -> LElement(h, function () -> toLazyList t) in 
		let rec order node=
			match node with
			| Leaf -> []
			| Node (v, l, r) -> (v::order l) @(order r) 
		in
		toLazyList (order tree)
		
	let rec postOrder tree=
		let rec toLazyList = function 
			| [] -> LKoniec 
			| h::t -> LElement(h, function () -> toLazyList t) in 
		let rec order node=
			match node with
			| Leaf -> []
			| Node (v, l, r) -> ((order l) @(order r)@[v]) in
		toLazyList (order tree)
	
	let rec toList = function
    | LKoniec -> []
    | LElement(x, xs) -> x :: toList (xs());;

	
	
		end ;; (*module BTree:TREE*)
	

let a=BTree.create();;
let a=BTree.push(1,a);;
let a=BTree.push(2,a);;
let a=BTree.push(-1,a);;

BTree.toList (BTree.inOrder a) ;;

let a=BTree.remove(2,a);;
BTree.toList (BTree.inOrder a) ;;
let b=BTree.find(-1,a);;


let t=BTree.create();;
let t=BTree.push(5,t) ;; 
let t=BTree.push(3,t)  ;;
let t=BTree.push(1,t)  ;;
let t=BTree.push(4,t)  ;;
let t=BTree.push(7,t)  ;;
  
BTree.preOrder(t) ;;
BTree.toList(BTree.preOrder(t) );;(* [5;3;1;4;7]*)  
BTree.postOrder(t) ;;
BTree.toList(BTree.postOrder(t));;(* [1;4;3;7;5]*)  
BTree.inOrder(t);;
BTree.toList(BTree.inOrder(t));; (* [1;3;4;5;7] *)
