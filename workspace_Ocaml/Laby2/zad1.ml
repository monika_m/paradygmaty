fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)

(fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)) 7;;
(fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)) 14;;
(fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)) 49;;
(fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)) 71;;
(fun x -> ((float_of_int(x)/.7.)-.float_of_int(int_of_float(float_of_int(x)/.7.)) = 0.)) 1;;

(fun x -> ((x/.7.)-.float_of_int(int_of_float(x/.7.)) = 0.)) 7.0;;
(fun x -> ((x/.7.)-.float_of_int(int_of_float(x/.7.)) = 0.)) 1.0;;

(fun x -> (x mod 7)=0) 1