 let div xs = 
	let rec lists(xs, even, pos, neg) =
		if (xs=[]) then (List.rev even, List.rev pos, List.rev neg)
		else if ((List.hd xs/.2.)-.float_of_int(int_of_float(List.hd xs/.2.)) = 0.) then lists(List.tl xs, List.hd xs::even, pos, neg)
		else if (List.hd xs>=0.) then lists(List.tl xs, even, List.hd xs::pos, neg)
		else lists(List.tl xs, even, pos, List.hd xs ::neg)
	in lists(xs, [], [], []);;
		
	div [1.0;2.0;3.0];;
	div [];;
	div [-1.; 2.0; 5.];;

 let divInt xs = 
	let rec lists(xs, even, pos, neg) =
		if (xs==[]) then (List.rev even, List.rev pos, List.rev neg)
		else if ((float_of_int(List.hd xs)/.2.)-.float_of_int(int_of_float(float_of_int(List.hd xs)/.2.)) = 0.) then lists(List.tl xs, List.hd xs::even, pos, neg)
		else if (List.hd xs>=0) then lists(List.tl xs, even, List.hd xs::pos, neg)
		else lists(List.tl xs, even, pos, List.hd xs ::neg)
	in lists(xs, [], [], []);;

	divInt [-1;0;1];;
	divInt[];;
