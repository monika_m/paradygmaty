let rec fun1 list=
	if (List.length list) >0 then ((List.hd list) +. fun1(List.tl list)) else 0.0;;
	
let list0 = [];;
fun1(list0);;
let list1= 2.0:: 3.1:: 24.15::[];;
fun1(list1);;
let list2 = 2.11:: 0.0 :: -2.11::[];;
fun1(list2);;