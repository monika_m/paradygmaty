module type QUEUE_MUT = 
	sig   
		type 'a t         (* The type of queues containing elements of type ['a]. *)   
		exception Empty of string         (* Raised when [first q] is applied to an empty queue [q]. *)   
		exception Full of string         (* Raised when [enqueue(x,q)] is applied to a full queue [q]. *)   
		val empty: int -> 'a t         (* [empty n] returns a new queue of length [n], initially empty. *)   
		val enqueue: 'a * 'a t -> unit       (* [enqueue (x,q)] adds the element [x] at the end of a queue [q]. *)   
		val dequeue: 'a t -> unit         (* [dequeue q] removes the first element in queue [q] *)           
		val first: 'a t -> 'a        (* [first q] returns the first element in queue [q] without removing              
																		it from the queue, or raises [Empty] if the queue is empty. *)    
		val isEmpty: 'a t -> bool         (* [isEmpty q] returns [true] if queue [q] is empty,             
																					otherwise returns [false]. *)   
		val isFull: 'a t -> bool         (* [isFull q] returns [true] if queue [q] is full,             
																				otherwise returns [false]. *) 
	end;;

module Queue_mut: QUEUE_MUT =
	struct
		type 'a t = {mutable f:int; mutable r:int; mutable tab: 'a option array }
		exception Empty of string
		exception Full of string
let empty n = { f=0; r=0; tab=Array.make (n+1) None } (*Array.create*)
let enqueue (el, xs) = 
	if (xs.r+1) mod  Array.length xs.tab =xs.f then raise (Full "pelna")
	else (
		xs.tab.(xs.r) <- Some el;
		xs.r <- (xs.r+1 mod  Array.length xs.tab)
		)
let dequeue xs = 
	(*if (xs.f = xs.r) then ()(*raise (Empty "pusta")*)*)
	if (xs.f <> xs.r) then(
		xs.tab.(xs.f) <- None;
		xs.f <- (xs.f+1 mod  Array.length xs.tab)
		)
let first xs=
	if (xs.f = xs.r) then  raise (Empty "pusta")
	else 
		match xs.tab.(xs.f) with
		| Some el -> el
		| None -> failwith "Implementation error"
let isEmpty xs= 
	xs.f=xs.r
let isFull xs=
	(xs.r+1) mod  Array.length xs.tab =xs.f
			
	end;;
	
let a = Queue_mut.empty 2;;
Queue_mut.isFull a;;
Queue_mut.first a;;
Queue_mut.enqueue(1, a);;
Queue_mut.first a;;
Queue_mut.isFull a;;
Queue_mut.enqueue(2, a);;
Queue_mut.isFull a;;
Queue_mut.dequeue a;;
Queue_mut.first a;;				
					
							
let rec a= None::None::a;;
let rec b= Some 2 :: a;;
