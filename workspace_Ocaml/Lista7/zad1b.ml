module type QUEUE_FUN = 
	sig   
		type 'a t   
		exception Empty of string   
		val empty: unit -> 'a t   
		val enqueue: 'a * 'a t -> 'a t   
		val dequeue: 'a t -> 'a t           
		val first: 'a t -> 'a   
		val isEmpty: 'a t -> bool 
	end;; 
 
module Queue:QUEUE_FUN=
	struct
		type 'a t= 'a list * 'a list
		exception Empty of string
		let empty() = ([],[])
		let enqueue(e1,(fst,snd))=
			if (fst=[]) then ([e1], [])
			else (fst, e1::snd)
		(*let enqueue(e1, q)= 
			match q with
			| (fst, snd) -> if (fst=[]) then ([e1], snd)
											else (fst, e1::snd)*)
		let dequeue(q) = 
			if (q=empty()) then empty()
			else 
				match q with
				| (fst, snd) -> if (List.tl fst = []) then (List.rev snd, [])
												else (List.tl fst, snd)
		let first = function
			| ([],[]) -> raise (Empty "Empty queue")
			| (fst, _) -> List.hd fst
		let isEmpty(q) = 
			if (q = empty()) then true
			else false
		end;;
		
	let list = Queue.empty();;
	let a=Queue.enqueue(1, list);;
	Queue.first(a);;