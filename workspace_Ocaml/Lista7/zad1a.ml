module type QUEUE_FUN = 
	sig   
		type 'a t   
		exception Empty of string   
		val empty: unit -> 'a t   
		val enqueue: 'a * 'a t -> 'a t   
		val dequeue: 'a t -> 'a t           
		val first: 'a t -> 'a   
		val isEmpty: 'a t -> bool 
	end;; 
 
module Queue:QUEUE_FUN=
	struct
		type 'a t= 'a list
		exception Empty of string
		let empty() = []
		let enqueue(e1, q)= q@[e1]
		let dequeue(q) = 
			if (q=empty()) then empty()
			else List.tl q
		let first(q) = 
			if (q= empty()) then raise (Empty "Empty queue")
			else List.hd q
		let isEmpty(q) = 
			if (q = empty()) then true
			else false
		end;;
		
	let list = Queue.empty();;
	let a=Queue.enqueue(1, list);;
	Queue.first(a);;