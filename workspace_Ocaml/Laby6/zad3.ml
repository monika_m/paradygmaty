type kolor = Czerwony | Pomaranczowy | Zolty | Zielony | Niebieski | Fioletowy;;

type teletubbies = {kolor:kolor; rzecz:(unit->string)};;

let getKolor t = t.kolor;;
let getRzecz t= t.rzecz();;

let tinky_winky = {kolor=Fioletowy; rzecz=function()->"torebka"};;
let dipsy = {kolor=Zielony; rzecz=function()->"kapelusz"};;
let lala = {kolor=Zolty; rzecz=function()->"pilka"};;
let po = {kolor=Czerwony; rzecz=function()->"hulajnoga"};;

getKolor po;;
getRzecz po;;
getRzecz dipsy;;
getKolor lala;;