let rec insert el= function
	| [] -> [el]
	| h::t -> if (h<= el) then h:: insert el t
						else el::h::t;;
						
insert 4 [0;1;2];;
insert 9.0 [];;
insert 'b' ['a'; 'c'];;