type 'a nlist = Koniec | Element of 'a * ('a nlist);; 
type 'a llist = LKoniec | LElement of 'a * (unit -> 'a llist);; 


let rec powiel nlist =
	let powiel2 el t=
		let rec powiel3 el count=
			if (count=0) then powiel t
			else Element(el,powiel3 el (count-1))
		in powiel3 el el
	in
	match nlist with
	| Koniec -> Koniec
	| Element(h, t) -> powiel2 h t;;

powiel (Element(2, Koniec));;
powiel (Element(0, Koniec));;
powiel Koniec;;
powiel (Element(1, Element(2, Element(5,Koniec))));;

let rec toLazyList = function [] -> LKoniec | h::t -> LElement(h, function () -> toLazyList t);;
let rec toList = function
    | LKoniec -> []
    | LElement(x, xs) -> x :: toList (xs());;

let rec lpowiel nlist =
	let powiel2 el t=
		let rec powiel3 el count=
			if (count=0) then lpowiel t
			else LElement(el, function() ->powiel3 el (count-1))
		in powiel3 el el
	in
	match nlist with
	| LKoniec -> LKoniec
	| LElement(h, t) -> powiel2 h (t());;

let a=lpowiel (toLazyList[2]);;
toList a;;
let b= lpowiel (toLazyList[0]);;
toList b;;
let c=lpowiel LKoniec;;
toList c;;
let d=lpowiel (toLazyList[1;2;5;1]);;
toList d;;

let rec powiell list =
	let powiel2 el=
		let rec powiel3 el count=
			if (count=0) then[]
			else el::powiel3 el (count-1)
		in powiel3 el el
	in
	match nlist with
	| [] -> []
	| h::t -> powiel2 h@powiel t;;

powiell [2];;
powiell [0];;
powiell [];;
powiell [1;2;5;1];;
