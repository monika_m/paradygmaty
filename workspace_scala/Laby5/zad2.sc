object zad2 {
  
 	def ldzialanie [A](list1:Stream[A], list2:Stream[A], op:Function2[A, A ,A]):Stream[A]=
 		(list1, list2) match {
 		case (Stream.Empty, _) => list2
 		case (_, Stream.Empty) => list1
 		case (h1#::t1, h2#::t2) => op (h1, h2)#:: ldzialanie (t1,t2,op)
 		}                                 //> ldzialanie: [A](list1: Stream[A], list2: Stream[A], op: (A, A) => A)Stream[A
                                                  //| ]
                                                   
 	def + (a:Double, b:Double):Double = a+b   //> + : (a: Double, b: Double)Double
 	def - (a:Double, b:Double):Double = a-b   //> - : (a: Double, b: Double)Double
 	def max (a:Int, b:Int):Int = if (a>b) a else b
                                                  //> max: (a: Int, b: Int)Int
 	
 	val r1= ldzialanie[Double] (Stream(1,2,3),Stream(4,5,6), +)
                                                  //> r1  : Stream[Double] = Stream(5.0, ?)
	r1 force                                  //> res0: scala.collection.immutable.Stream[Double] = Stream(5.0, 7.0, 9.0)
  val r2= ldzialanie (Stream(1.0,2,3),Stream.Empty, -)
                                                  //> r2  : Stream[Double] = Stream(1.0, ?)
  r2 force                                        //> res1: scala.collection.immutable.Stream[Double] = Stream(1.0, 2.0, 3.0)
  val r3 = ldzialanie(Stream(1),Stream(0,2,3), max)
                                                  //> r3  : Stream[Int] = Stream(1, ?)
 	r3 force                                  //> res2: scala.collection.immutable.Stream[Int] = Stream(1, 2, 3)
 	
}