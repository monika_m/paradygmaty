object zad3 {
// def sumProdl(list:List[Int]) = (list foldLeft (0,1)) ((acc, h) => (acc._1 + h, acc._2 * h))
 
 def sumProd(xs:List[Int]) = xs.foldLeft (0,1) ((acc, h) => (acc._1 + h, acc._2 * h))
                                                  //> sumProd: (xs: List[Int])(Int, Int)
 															//(0,1) jest poczatkowa wartoscia akumulatora
 
def sumProd2(xs:List[Int]) = xs.foldLeft (0,1) ((acc, i) => {
																			val (s,p)=acc
																			(s+i,p*i)
																			})
                                                  //> sumProd2: (xs: List[Int])(Int, Int)
 
 
 
 //def insert[A](poprzedza: A=>A=>Boolean)(elem: A)(xs: List[A]): List[A] =  xs match { case Nil   => elem::Nil case y::ys => if (poprzedza (elem) (y)) elem::xs else y::insert(poprzedza)(elem)(ys)  }
  def insert[A](poprzedza: (A,A)=>Boolean, elem: A, xs: List[A]): List[A] =
  xs match {
  case Nil   => elem::Nil
  case y::ys => if (poprzedza (elem, y)) elem::xs else y::insert(poprzedza, elem, ys)
  }                                               //> insert: [A](poprzedza: (A, A) => Boolean, elem: A, xs: List[A])List[A]
}