object zad5 {

  def insertionsort[A](pred:(A, A) => Boolean, xs:List[A]) = {
		def insert(element:A, newList:List[A]):List[A] =
			newList match {
				case Nil => List(element)
				case h :: t =>
						if (pred(h, element)) h :: insert(element, t)
						else element :: newList
			}
	xs.foldLeft (List[A]()) ((list_acc, element)=>insert(element, list_acc))
	}                                         //> insertionsort: [A](pred: (A, A) => Boolean, xs: List[A])List[A]
	
	def isSmaller (a:(Int,Int),b:(Int,Int)):Boolean = a._1<=b._1
                                                  //> isSmaller: (a: (Int, Int), b: (Int, Int))Boolean
	
	insertionsort(isSmaller, List((10,0), (2,1), (9,1), (2,0)))
                                                  //> res0: List[(Int, Int)] = List((2,1), (2,0), (9,1), (10,0))
    
    
   def mergesort[A](pred:(A, A) => Boolean, xs: List[A]): List[A] = {
			val partition = xs.length / 2 //O(n)
			if (partition == 0) xs
			else {
				def merge(list1: List[A], list2: List[A]): List[A] =
					(list1, list2) match {
					case(Nil, list2) => list2
					case(list1, Nil) => list1
					case(head1 :: tail1, head2 :: tail2) =>
										if (pred(head1, head2)) head1::merge(tail1, list2)
										else head2 :: merge(list1, tail2)
					}
				val (left, right) = xs.splitAt(partition)//O(1)
				merge(mergesort(pred, left), mergesort(pred, right))
			}
	}                                         //> mergesort: [A](pred: (A, A) => Boolean, xs: List[A])List[A]
}