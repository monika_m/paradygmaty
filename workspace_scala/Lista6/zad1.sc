object zad1 {
  val a =1                                        //> a  : Int = 1
 def whileLoop2 (warunek:Boolean, wyr:Unit):Unit=
  	while (warunek) wyr                       //> whileLoop2: (warunek: Boolean, wyr: Unit)Unit
  	
   def whileLoop(condition: =>Boolean)(expression: =>Unit):Unit= {
		if (condition) {
			expression
			whileLoop(condition)(expression)
		}
	}                                         //> whileLoop: (condition: => Boolean)(expression: => Unit)Unit
	 
	var x=0                                   //> x  : Int = 0
	whileLoop(x<3){
		println(x)
		x=x+1
		}                                 //> 0
                                                  //| 1
                                                  //| 2
  var y=0                                         //> y  : Int = 0
	whileLoop(y<3)({
		println(y)
		y=y+1
		})                                //> 0
                                                  //| 1
                                                  //| 2
  	
 /* def war (x:Int):Boolean = (x>0)
  def wyr (x:Int):Unit = {
    System.out.print(x) }
  */
  //whileLoop (war(1), wyr(1))
    
 
}