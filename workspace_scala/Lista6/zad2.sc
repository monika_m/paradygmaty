object zad2 {
	def swap (tab:Array[Int], i:Int, j:Int):Unit = {
		val aux = tab(i)
		tab(i) = tab(j);
		tab(j) = aux;;
		}                                 //> swap: (tab#92342: Array#778[Int#892], i#92343: Int#892, j#92344: Int#892)Uni
                                                  //| t#1795
		
	val tab=Array(1,2,3)                      //> tab  : Array#778[Int#892] = Array(1, 2, 3)
	swap (tab, 1,2)
	tab                                       //> res0: Array#778[Int#892] = Array(1, 3, 2)
	 
	def partition(tab:Array[Int], l:Int, r:Int):(Int,Int) = {
		var i=l
		var j=r
		var pivot= tab((l+r)/2);;
		while (i <= j) {
			while (tab(i) < pivot)
				i+= 1
			while (pivot < tab(j))
				j-=1
			if (i <= j) {
					swap (tab,i, j)
					i+=1
					j-=1
					}
			}
			(i,j)
		}                                 //> partition: (tab#92397: Array#778[Int#892], l#92398: Int#892, r#92399: Int#89
                                                  //| 2)(Int#892, Int#892)
		
	def quick (tab:Array[Int], l:Int, r:Int):Unit =
		if (l < r){
			val (i,j) = partition (tab, l, r)
			if (j-l < r-i) {
				val _ = quick (tab, l, j)
				quick (tab, i, r)
				}
			else {
			val _ = quick (tab, i, r)
			quick (tab, l, j)
			}
		}
		else ()                           //> quick: (tab#92436: Array#778[Int#892], l#92437: Int#892, r#92438: Int#892)Un
                                                  //| it#1795
  def quicksort (tab:Array[Int]):Unit =
  quick (tab, 0, ((tab.length )-1))               //> quicksort: (tab#92461: Array#778[Int#892])Unit#1795
  
  var tab2= Array(0,2,1,9,3)                      //> tab2  : Array#778[Int#892] = Array(0, 2, 1, 9, 3)
  quicksort (tab2)
  tab2                                            //> res1: Array#778[Int#892] = Array(0, 1, 2, 3, 9)
  
	
}