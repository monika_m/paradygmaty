package Cylinder

class Point(protected var x: Double = 0.0, protected var y: Double = 0.0) {

  def setX(newX: Double): this.type = {
    x = newX
    this
  }

  def setY(newY: Double): this.type = {
    y = newY
    this
  }

  override def toString = s"x: $x y: $y "
}