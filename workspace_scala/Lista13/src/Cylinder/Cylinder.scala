package Cylinder

class Cylinder(x: Double = 0.0, y: Double = 0.0, radius: Double = 1.0, private var h: Double = 2.0) extends Circle(x, y, radius) {
  require(radius > 0 && h > 0)

  def setHeight(newHeight: Double): this.type = {
    require(newHeight > 0)
    h = newHeight

    this
  }

  override def toString = super.toString + s"height: $h"
}
/*val point = new Point
val circle = new Circle
val cylinder = new Cylinder
point.toString
circle.toString
cylinder.toString


cylinder.setX(12.0).setY(10.0).setRadius(3.0).setHeight(5.0)
cylinder.toString

//point: Point = x: 0.0 y: 0.0
circle: Circle = x: 0.0 y: 0.0 r: 1.0
cylinder: Cylinder = x: 12.0 y: 10.0 r: 3.0 height: 5.0
res4: String = "x: 12.0 y: 10.0 r: 3.0 height: 5.0 "
*/