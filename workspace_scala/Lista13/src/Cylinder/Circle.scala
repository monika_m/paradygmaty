package Cylinder

class Circle(x: Double = 0.0, y: Double = 0.0, protected var r: Double = 1.0) extends Point(x, y) {
  require(r > 0)
  def setRadius(newRadius: Double): this.type = {
    require(newRadius > 0)
    r = newRadius
    this
  }

  override def toString = super.toString + s"r: $r "
}