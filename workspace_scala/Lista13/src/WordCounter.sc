import scala.collection.mutable.Map
object WordCouner{
def wordCounter(text: String): Map[String,Int] ={
  Map(text.split(" ").groupBy(identity).mapValues(_.length).toSeq:_*)
}                                                 //> wordCounter: (text: String)scala.collection.mutable.Map[String,Int]

val text = "A AA B BB C CC AA C CC BB BB BB CC"   //> text  : String = A AA B BB C CC AA C CC BB BB BB CC

wordCounter(text)                                 //> res0: scala.collection.mutable.Map[String,Int] = Map(A -> 1, AA -> 2, C -> 2
                                                  //| , BB -> 4, B -> 1, CC -> 3)
text.split(" ")                                   //> res1: Array[String] = Array(A, AA, B, BB, C, CC, AA, C, CC, BB, BB, BB, CC)
                                                  //| 

def wordCounter2(text: String): Map[String, Int] = {
  val counter: Map[String, Int] = Map().withDefaultValue(0)
  text.split(' ').foreach(word => counter(word) += 1)
  counter
}                                                 //> wordCounter2: (text: String)scala.collection.mutable.Map[String,Int]

wordCounter2(text)                                //> res2: scala.collection.mutable.Map[String,Int] = Map(A -> 1, AA -> 2, C -> 2
                                                  //| , BB -> 4, B -> 1, CC -> 3)
}