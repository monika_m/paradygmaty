

abstract class AbstractPair {
  type A
  type B
  
  var fst:A=_
  var snd:B=_
  
  override def toString() = s"$fst, $snd"
}

object Main2 extends App{
  val p1 = new AbstractPair{type A=String
    type B=Int
    fst="a"
    snd=2} //  p1: AbstractPair{type A = String; type B = Int} = a, 2
}