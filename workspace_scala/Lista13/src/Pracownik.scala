
import Pracownik._

class Pracownik (val nazwisko:String, var zwolniony:Boolean=false) {
  liczbaPrac = liczbaPrac+1;
  
  def zwolnij = {
    liczbaPrac -=1
    zwolniony=true
  }
  
  override def toString()= s"Pracownik: Nazwisko: $nazwisko, zwolniony? $zwolniony"
}

object Pracownik {
  private var liczbaPrac = 0
  
  def liczbaPracownikow:Int = liczbaPrac
}