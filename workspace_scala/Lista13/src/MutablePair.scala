

class MutablePair[A,B](var fst:A, var snd:B) {
  
  override def toString() = s"$fst, $snd"
}


object Main extends App{
  val p1 = new MutablePair("a",2) // p1: MutablePair[String,Int] = a, 2
}