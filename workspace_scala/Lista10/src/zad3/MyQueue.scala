package zad3

class EmptyQueueException(msg: String) extends Exception(msg)

class MyQueue [+T] private(private val rep: (List[T], List[T])) { 
   def enqueue[S >: T](x: S) = 
    rep match{
     case (List(), _) => new MyQueue(List(x),List()) 
     case (fst, snd) => new MyQueue(fst, x::snd)
   }
   def dequeue()=
     rep match{
     case (List(), List()) => (List(), List())
     case (fst, snd) =>
       if (fst.tail ==List()) (snd.reverse, List())
       else (fst.tail, snd)
   }
   def first()=
     rep match{
     case (List(), List()) => throw new EmptyQueueException("Pusta kolejka")
     case (fst, _) => fst.head
   }
   def isEmpty() = rep==(List(), List())
}
object MyQueue {                                                                                   
  // obiekt towarzyszący 
  def apply[T](xs: T*) = new MyQueue[T](xs.toList, List()) 
  def empty[T] = new MyQueue[T](Nil,Nil) 
}