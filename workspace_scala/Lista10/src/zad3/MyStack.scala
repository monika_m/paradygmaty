package zad3

class UnderflowException(msg: String) extends Exception(msg)

class MyStack[+T] private(private val rep: List[T]) { 
  def push[S >: T](x: S) = new MyStack(x::rep) // push: S => MyStack[S] / / poprzednio było push: T => MyStack[T] 
 // def push[S >: T](x: S) = new MyStack(x::rep) 
  def top = 
    rep match{ 
    case x::_ => x 
    case Nil  => throw new UnderflowException("Empty stack") 
    } 
  def pop = 
    rep match{ 
    case _::xs => new MyStack(xs) 
    case Nil    => this 
    } 
  def isEmpty= rep == Nil
}
object MyStack {                                                                                   
  // obiekt towarzyszący 
  def apply[T](xs: T*) = new MyStack[T](xs.toList.reverse) //Umieszczając gwiazdkę po typie ostatniego argumentu funkcji wskazujemy, że może on być powtórzony, czyli funkcja może przyjmować zmienną liczbę argumentów
  def empty[T] = new MyStack[T](Nil) 
}