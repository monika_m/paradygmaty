package zad1

//Klasa GenericCellImm kompiluje się jako klasa inwariantna i kowariantna
//inwariantna
//class GenericCellImm[T](val x: T) {} //defined class GenericCellImm

class GenericCellImm[+T](val x: T) {} //defined class GenericCellImm
//kowariantna

//Natomiast klasa GenericCellMut kompiluje się tylko jako klasa inwariantna.
class GenericCellMut[T](var x: T) {} //defined class GenericCellMut 
 
//Wersja kowariantna powoduje błąd kompilacji. 
//class GenericCellMut[+T] (var x: T) { } 
//<console>:12: error: covariant type T occurs in contravariant position in type T of value x_= 
 
//a) Wyjaśnij powód tego błędu. 

/* => var -> akcesor i mutator (foo oraz foo_=)
 * 		val -> akcesor (foo)
 *  typy argumentów mogą występować wyłącznie na pozycjach kontrawariantnych(-S),
 *   natomiast typy wyników – na pozycjach kowariantnych (+T). 
 *  -Kontrawariantne argumenty funkcji są odbiornikami wartości z otoczenia. 
 *  +Kowariantny wynik funkcji jest źródłem wartości dla otoczenia
 */
/*a)	Kompilator automatycznie tworzy dla modyfikowalnej zmiennej VAR akcesor (getter) i mutator (setter).
 *  Mutator przyjmuje argument na pozycji kowariantnej, a powinien przyjąć na pozycji kontrawariantnej 
 *  (bo ZAPIS!).*/
/*Zwykle taki parametr może być kowariantny tylko wtedy, kiedy kolekcja jest wyłącznie do odczytu,
 *  czyli ma tylko metody pobierające wartości jej elementów (getters); może on być kontrawarianty 
 *  tylko wtedy, kiedy kolekcja jest wyłącznie do zapisu, czyli ma tylko metody zmieniające wartości
 *   jej elementów (setters). Jeśli dopuszczalne są zarówno operacje odczytu, jak i zapisu, 
 *   to kolekcja musi być inwariantna.*/

/* a) Jako klasa inwariantna jest to oczywiste – spodziewamy się typu T i w argumencie dostajemy typ
T, więc wszystko bez problemu zadziała. Jako klasa kowariantna – argument jest
typu T, który jest podtypem typu klasy, np. new GenericCellImm[Number](5.0) lub
new GenericCellImm[Number](5), gdzie klasa jest typu Number, a argument typu Int lub Double
(widać, że argument jest podtypem typu klasy). Dla val czyli wartości stałej jako, że argument nie
zmieni na pewno swojej zawartości wszystko jest bezpieczne – wiadomo, że typ argumentu nie
zmieni się nagle, np. z Int na Double. W przypadku gdy argument jest typu var czyli jest wartością
zmienną w przypadku klasy kowariantnej w podanym przykładzie jest możliwość zmiany argumentu,
np. z Int na Double, co spowoduje problem z typami. 

Podobnie jeśli klasa byłaby, np. generyczną
tablicą, a jak wiadomo tablice są homogeniczne, więc ustawienie typu tablicy na Any umożliwiłoby
wrzucenie tam razem, np. Intów i Stringów, które są różnymi typami. W podobnych przypadkach
Java wyrzuca wyjątek ArrayStoreException, Scala nie dopuszcza do takich sytuacji dając błąd
kompilacji w przypadku próby skompilowania kowariantnej klasy, której argumentem konstruktora
głównego jest wartość zmienna.*/

//b) Czy można się pozbyć tego błędu? Uzasadnij swoją odpowiedź. 
/*
Tego błędu się można pozbyć tylko poprzez zmianę var na val lub poprzez zmianę klasy
kowariantnej na inwariantną czyli stosując jeden z wcześniej podanych wariantów.
*/
/*b)	Usunięcie „+” przy typie generycznym T. Wtedy parametr typu będzie inwariantny, więc zarówno akcesor,
 *  który odczytuje będzie mógł zwrócić typ T, jak i mutator który zapisuje ten sam typ T będzie mógł
 *   zmienić wartość zmiennej x.
Można też zmienić VAR na VAL. Wtedy kompilator generuje automatycznie sam akcesor, który zwraca k
owariantnie, więc program wtedy się skompiluje.
Kolejnym sposobem jest nadanie modyfikatora dostępu zmiennej x jako private[this]. Kompilator Scali
 sprawdza wariantność parametrów typowych z wyjątkiem parametrów o tym właśnie dostępie. Dostęp do 
 zmiennych z tego samego obiektu, w którym są one zdefiniowane NIE powoduje problemów z wariantnością.*/

//c) Czy wersja kontrawariantna skompiluje się? Uzasadnij swoją odpowiedź. 
//class GenericCellMut[-T] (var x: T) { } 
 //nie: contravariant type T occurs in covariant position in type ⇒ T of variable x