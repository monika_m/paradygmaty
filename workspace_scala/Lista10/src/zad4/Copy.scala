package zad4
import scala.collection.mutable
object Copy {
  /* def  copy(List<-T> dest, List<+T> src) {
    for (T iter: src) {
      dest.update(i, src.get(i));
      }
    }
}*/

  def copy[T](dest: mutable.Seq[T], src: mutable.Seq[T]): Unit = {
    require(dest.length >= src.length)
    var index = 0
    src.foreach(element => {
      dest.update(index, element)
      index += 1
    })
  }
}
/*scala.collection.mutable.Seq

A subtrait of collection.Seq which represents sequences that can be mutated.

Sequences are special cases of iterable collections of class Iterable.Unlike iterables, sequences always have a defined order of elements.Sequences provide a method apply for indexing. Indices range from 0 up to the length ofa sequence. Sequences support a number of methods to find occurrences of elements or subsequences, including segmentLength, prefixLength, indexWhere, indexOf, lastIndexWhere, lastIndexOf, startsWith, endsWith, indexOfSlice.

Another way to see a sequence is as a PartialFunction from Int valuesto the element type of the sequence. The isDefinedAt method of a sequencereturns true for the interval from 0 until length.

Sequences can be accessed in reverse order of their elements, using methods reverse and reverseIterator.

Sequences have two principal subtraits, IndexedSeq and LinearSeq, which give different guarantees for performance.An IndexedSeq provides fast random-access of elements and a fast length operation.A LinearSeq provides fast access only to the first element via head, but alsohas a fast tail operation.

The class adds an update method to collection.Seq.
*/