package zad2

//abstract class Sequence[+A] {
//abstract class Sequence[A] {
 // def append(x: Sequence[A]): Sequence[A]
  // <console>:14: error: covariant type A occurs in contravariant position in type Sequence[A] of value x
  /*typy argumentów mogą występować wyłącznie na pozycjach kontrawariantnych(-S), super nadtyp
	 *natomiast typy wyników – na pozycjach kowariantnych (+T). extends podtyp
	 * w tym przypadku kowariantny typ A jest także argumentem, co się z tym kłóci
	 * jedynym rozwiązaniem jest typ inwariantny*/
  
  /*Aby abstrakcyjna klasa Sequence pozostała kowariantna, to metoda APPEND musi przyjmować wyłącznie 
   * argumenty, które są nadtypami typu T. Trzeba więc sparametryzować metodę dodatkowym typem S, który 
   * jest nadtypem typu A. Metoda zwraca sekwencję typu A, ale może równie dobrze zwracać typu S 
   * (tam gdzie oczekiwany jest typ S, może być użyty typ A).*/
  
  
  // B nadtypem A
  abstract class Sequence[+A] {
  def append[B >:A](x: Sequence[B]): Sequence[B]
                                            //A tez sie kompiluje
}
