package zad1

import Player.{Ping, Pong}
import akka.actor.{Actor, ActorSystem, PoisonPill, Props}

class Player(name: String, timesToBounce: Int) extends Actor {
  private var counter = timesToBounce

  override def receive : PartialFunction[Any,Unit] = {
    case Ping => {
      if (counter > 0) {
        println(s"$name : ping " + counter)
        counter -= 1
        sender() ! Pong
      } else {
        sender() ! PoisonPill
        self ! PoisonPill
      }
    }
    case Pong => {
      if (counter > 0) {
        println(s"$name : pong " + counter)
        counter -= 1
        sender() ! Ping
      } else {
        sender() ! PoisonPill
        self ! PoisonPill
//        context.system.terminate()
      }
    }
    case _ => println("Wrong communication!")
  }
}

object Player {
  def props(name: String, timesToBounce: Int) = Props(classOf[Player], name, timesToBounce)
  case object Ping
  case object Pong
}

object PingPong extends App {
  
  val timesToBounce = 4
  val mainSystem = ActorSystem("pingpong")
  val player1 = mainSystem.actorOf(Player.props("player1", timesToBounce))
  val player2 = mainSystem.actorOf(Player.props("player2", timesToBounce))
  player1.tell(Ping, player2) //player2-adres zwrotny niby nadawcy, nadawcą jest main wysyła do playera1, ze zwrotnym do player2

}
