package zad2

  // wersja z limitem odbić
import scala.util.Random
import Player.Ball
import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class Player(val num: Int, val players: Array[ActorRef]) extends Actor {
  val random = new Random()
  override def receive: Receive = {
    case Ball(count) => {
      if (count > 0) {
        println("player nr " + num + " with throw nr " + count)
        var index = 0
        // choose other player but not self
        do {
          index = random.nextInt(players.length) // from 0 inclusive to specified number exclusive
        } while (index == num)
        players(index) ! Ball(count - 1)
      } else {
        context.system.terminate()
      }
    }
    case _ => println("Wrong communication!")
  }
}

object Player {
  def props(num: Int, players: Array[ActorRef]) = Props(classOf[Player], num, players)
  case class Ball(count: Int)
}

object ThrowingBall extends App {
  val playersList = new Array[ActorRef](3)
  val system = ActorSystem("throwing")
  val player1: ActorRef = system.actorOf(Player.props(0, playersList))
  playersList.update(0, player1)
  val player2: ActorRef = system.actorOf(Player.props(1, playersList))
  playersList.update(1, player2)
  val player3: ActorRef = system.actorOf(Player.props(2, playersList))
  playersList.update(2, player3)
  player1 ! Ball(10)
}
/*player nr 0 with throw nr 10
player nr 1 with throw nr 9
player nr 0 with throw nr 8
player nr 1 with throw nr 7
player nr 2 with throw nr 6
player nr 0 with throw nr 5
player nr 2 with throw nr 4
player nr 0 with throw nr 3
player nr 2 with throw nr 2
player nr 0 with throw nr 1*/
