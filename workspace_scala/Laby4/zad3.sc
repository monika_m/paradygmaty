object zad3 {
 
 	sealed trait Typ[+A]
 	case class Komputer[A](list: List[A]) extends Typ[A]
 	
 	def contains (komp: Typ[Int], x:Int):Boolean={
 		def isIn (list:List[Int], x:Int):Boolean=
 			list match{
 				case Nil => false
 				case h::t => if(h==x) true else isIn(t,x)
 				}
 		 komp match{
 		 case Komputer(list)=> isIn(list, x)
 		 }
 	 }                                        //> contains: (komp: zad3.Typ[Int], x: Int)Boolean
 	 
 	 val komp = Komputer[Int](List(0,1,2))    //> komp  : zad3.Komputer[Int] = Komputer(List(0, 1, 2))
 	 contains (komp, 2)                       //> res0: Boolean = true
 	 contains (komp, 3)                       //> res1: Boolean = false
}