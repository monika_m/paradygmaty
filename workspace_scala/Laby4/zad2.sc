object zad2 {
  sealed trait Funct
	case class Add(a: Funct, b:Funct) extends Funct
	case class Neg(n: Funct) extends Funct
	case class N(n:Int) extends Funct
	
	def calculate (f: Funct):Int =
		f match {
		case Add(a,b) => calculate(a)+calculate(b)
		case Neg(n) => -calculate(n)
		case N(n) => n
   }                                              //> calculate: (f: zad2.Funct)Int
   
  val add = Add(N(1),N(2))                        //> add  : zad2.Add = Add(N(1),N(2))
  val neg = Neg (N(1))                            //> neg  : zad2.Neg = Neg(N(1))
  val nested = Add(Neg(N(1)),Add(N(1),N(2)))      //> nested  : zad2.Add = Add(Neg(N(1)),Add(N(1),N(2)))
  calculate(add)                                  //> res0: Int = 3
  calculate(neg)                                  //> res1: Int = -1
  calculate (nested)                              //> res2: Int = 2
 }