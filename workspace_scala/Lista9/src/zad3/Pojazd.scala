package zad3

class Pojazd (val producent:String, val model:String, val rok_produkcji:Int=(-1), var nr_rejestracyny:String=""){
  //W Scali pierwszą akcją konstruktora pomocniczego musi być wywołanie innego konstruktora tej samej klasy, zdefiniowanego przed nim (tekstowo).  Każdy konstruktor pomocniczy musi mieć listę argumentów (być może pustą).
 /* def this(producent:String, model:String){
    this(producent, model, -1, "")
  }*/
  
  /*def this(producent:String, model:String, rok_produkcji:Int){
    this(producent, model, rok_produkcji, "")
  }*/

  def this(producent:String, model:String, nr_rejestracyjny:String){
    this(producent, model, -1, nr_rejestracyjny)
  }
}