package zad4
//obiekt autonomiczny/standalone - kontener na metody i składowe statyczne
object UzycieWyjatkow {
  def main(args: Array[String]) {
    try {
      metoda1()
    } catch {
      case e: Exception =>
        System.err.println(e.getMessage() + "\n")
        e.printStackTrace()
    }
  }
  /*
Wyjatek zgloszony w metoda3

java.lang.Exception: Wyjatek zgloszony w metoda3
	at zad4.UzycieWyjatkow$.metoda3(UzycieWyjatkow.scala:23)
	//	klasa syntetyczna, skompilowany obiekt .class
	at zad4.UzycieWyjatkow$.metoda2(UzycieWyjatkow.scala:19)
	at zad4.UzycieWyjatkow$.metoda1(UzycieWyjatkow.scala:15)
	at zad4.UzycieWyjatkow$.main(UzycieWyjatkow.scala:6)
	at zad4.UzycieWyjatkow.main(UzycieWyjatkow.scala)*/
  def metoda1() = metoda2()

  def metoda2() = metoda3()

  def metoda3() {
    throw new Exception("Wyjatek zgloszony w metoda3");
  }
} 


/*public class UzycieWyjatkow {   
 * public static void main( String args[] ) {      
 * try {          
 * metoda1();       
 * } catch ( Exception e ) {          
 * System.err.println( e.getMessage() + "\n" );         
 * e.printStackTrace();      
 * }   
 * } 
 * public static void metoda1() throws Exception {     
 *  metoda2();     
 *  }    
 *  public static void metoda2() throws Exception {      
 *  metoda3();     
 *  }    
 *  public static void metoda3() throws Exception {      
 *  throw new Exception( "Wyjatek zgloszony w metoda3" );    
 *  } }*/
 