

package zad1

class Time(private[this] var h: Int) {//private kryje domyślne mutatory i akcesory
  if (h < 0) h = 0//unit

  def hour: Int = h 
  def hour_=(x: Int)//:Unit=
  {
    if (x < 0) h = 0
    else h = x
  }

}
object Time {
  def apply(hour: Int) = new Time(hour)
}

//val t = new Time(2)
//t.hour_=3
//t.hour_=(3)
