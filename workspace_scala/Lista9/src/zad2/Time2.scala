

package zad2

class Time2 (private var h:Int=0, private var m:Int=0) {
 // if (h < 0 && h>24) throw new IllegalArgumentException
//  else if (m<0 && m>60) throw new IllegalArgumentException
require(0 <= h && h < 24, "komunikat bledu")
require(0 <= m && m < 60) //kod poza metodami to kod konstruktora
  //require mówimy, że użytkownik może podać złe argumenty, przy assert -twórca kodu, nasz kod musi coś zrobić
  def hour: Int = h
  def hour_=(x: Int) {
    if (x >= 0 && x<24) h = x
    else throw new IllegalArgumentException
  }
  def min: Int = m
  def min_=(x: Int) {
    if (x >= 0 && x<60) m = x
    else throw new IllegalArgumentException
  }
   def before(other: Time2): Boolean={
     if (hour<other.hour) true
     else if (hour>other.hour) false
     else min<other.min
   }
}