

package zad2

class Time3(h:Int=0, m:Int=0) {
require(0 <= h && h < 24)
require(0 <= m && m < 60)
  
private[this] var mins= 60*h+m
//operator dostępu-tylko z danego momentu
  def hour: Int = mins/60
  def hour_=(x: Int) {
    if (x >= 0 && x<24) mins=(mins%60)+(x*60)
    else throw new IllegalArgumentException
  }
  def min: Int = mins%60
  def min_=(x: Int) {
    if (x >= 0 && x<60) mins=mins-min+x
    else throw new IllegalArgumentException
  }
   def before(other: Time3): Boolean={
     if (hour<other.hour) true
     else if (hour>other.hour) false
     else min<other.min
   }

}