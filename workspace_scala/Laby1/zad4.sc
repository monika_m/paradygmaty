object zad4 {
  def fun4(a: Double, b:Double, c:Double):Boolean=
     if (a<b) false else if (b<c) false else true //> fun4: (a: Double, b: Double, c: Double)Boolean
     
   fun4(10.0, 3.0, 2.0)                           //> res0: Boolean = true
   fun4(10.0, 3.0, 4.0)                           //> res1: Boolean = false
   fun4(1,2,3)                                    //> res2: Boolean = false
   fun4(3,2,1)                                    //> res3: Boolean = true
   
   
       
    
    
    
    
    
  def fun3a(list: List[Int], n: Int, lret: List[Int]): List[Int]=
      if(list.length>0) {
      if(list.head>n) fun3a(list.tail, n, lret:::List(list.head))
      else fun3a(list.tail, n, lret)
      } else lret                                 //> fun3a: (list: List[Int], n: Int, lret: List[Int])List[Int]
      
   def fun3B(list: List[Int], n: Int): List[Int]=
      fun3a(list, n, List[Int]())                 //> fun3B: (list: List[Int], n: Int)List[Int]
     
    def fun3aT(list: List[Int], n: Int, lret: Array[Int]): Array[Int]=
       if(list.length>0) {
      if(list.head>n) fun3aT(list.tail, n, lret:+(list.head))
      else fun3aT(list.tail, n, lret)  } else lret//> fun3aT: (list: List[Int], n: Int, lret: Array[Int])Array[Int]
    def fun3T(list: List[Int], n: Int): Array[Int]=
      fun3aT(list, n, Array[Int]())               //> fun3T: (list: List[Int], n: Int)Array[Int]
  
   
   
   
   
   
   
   
   
   
   
   
   def fun5(x:Int, list: List[Int]):Int=
   	if (list==Nil) 0 else if (list.head==x) 1 + fun5(x, list.tail) else 0 + fun5(x,list.tail)
                                                  //> fun5: (x: Int, list: List[Int])Int
   	
}