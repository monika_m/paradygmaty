object zad5 {
   def fun5(x:Char, list: List[Char]):Int=
   	if (list==Nil) 0 else if (list.head==x) 0 + fun5(x, list.tail) else 1 + fun5(x,list.tail)
                                                  //> fun5: (x: Char, list: List[Char])Int
   val list1 = 'a'::'b'::'a'::Nil                 //> list1  : List[Char] = List(a, b, a)
   fun5('a', list1)                               //> res0: Int = 1
   fun5('b', list1)                               //> res1: Int = 2
   fun5('c', list1)                               //> res2: Int = 3
   val list0 = List[Char]()                       //> list0  : List[Char] = List()
   	fun5('a', list0)                          //> res3: Int = 0
  }