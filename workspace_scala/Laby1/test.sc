object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
1-2*3                                             //> res0: Int = -5
2.5 + 3.5                                         //> res1: Double(6.0) = 6.0
(1 > 2) == false                                  //> res2: Boolean = true

val k3 = (3+4, 2.0, 2<4)                          //> k3  : (Int, Double, Boolean) = (7,2.0,true)
k3._1                                             //> res3: Int = 7
k3._3                                             //> res4: Boolean = true
k3 == (8-1, 2.0 , 2==2)                           //> res5: Boolean = true

val k2 = (3+4, (2.0 , 2<4))                       //> k2  : (Int, (Double, Boolean)) = (7,(2.0,true))
(k2._2)._1                                        //> res6: Double = 2.0

val x1=7.5                                        //> x1  : Double = 7.5
val xs= 1.0 :: x1 :: 2.5 :: Nil                   //> xs  : List[Double] = List(1.0, 7.5, 2.5)

val fun2 = (tab: Array[Double]) => {
	var x=0.0
	for (y <- tab) x=x+y}                     //> fun2  : Array[Double] => Unit = test$$$Lambda$11/1225373914@3a03464
	
	val Tablica: Array[Double]= Array(1.0, 2.0)
                                                  //> Tablica  : Array[Double] = Array(1.0, 2.0)
	def fun3(tab: Array[Double]):Double=
      if (tab.length > 0) tab.head+fun3(tab.drop(1)) else 0
                                                  //> fun3: (tab: Array[Double])Double
      fun3(Tablica)                               //> res7: Double = 3.0
      val Tab: Array[Double]=Array(9.0, 1.1, 3.45)//> Tab  : Array[Double] = Array(9.0, 1.1, 3.45)
      fun3(Tab)                                   //> res8: Double = 13.55
      val T: Array[Double]=Array()                //> T  : Array[Double] = Array()
      fun3(T)                                     //> res9: Double = 0.0
                                               
}