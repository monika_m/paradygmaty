object zad3 {

 def  fun3(list: List[Int], n: Int): List[Int]=
   		if (list==Nil) List[Int]() else {
   		if (list.head>n) list.head::fun3(list.tail, n)
   		else fun3(list.tail,n)}           //> fun3: (list: List[Int], n: Int)List[Int]
    
    
    val lista1 = 1::3::120::23::Nil               //> lista1  : List[Int] = List(1, 3, 120, 23)
    fun3(lista1, 10)                              //> res0: List[Int] = List(120, 23)
    fun3(lista1, 0)                               //> res1: List[Int] = List(1, 3, 120, 23)
    fun3(lista1, 1200)                            //> res2: List[Int] = List()
    val lista0 : List[Int] = List()               //> lista0  : List[Int] = List()
    fun3(lista0,0)                                //> res3: List[Int] = List()
        
   
}