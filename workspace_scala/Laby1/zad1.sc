object zad1 {
   def fun1(list: List[Double]):Double=
      if (list.length >0) list.head+fun1(list.tail) else 0
                                                  //> fun1: (list: List[Double])Double
  		val list1 = 1.0:: 2.0:: 1.55::Nil //> list1  : List[Double] = List(1.0, 2.0, 1.55)
 			fun1(list1)               //> res0: Double = 4.55
  		val list0: List[Double] = List()  //> list0  : List[Double] = List()
  		fun1(list0)                       //> res1: Double = 0.0
  		val list2: List[Double] = List(1.1, 0.01, 5.5, 101.0, 11.0)
                                                  //> list2  : List[Double] = List(1.1, 0.01, 5.5, 101.0, 11.0)
  		fun1(list2)                       //> res2: Double = 118.61
}