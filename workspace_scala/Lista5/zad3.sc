object zad3 {
  sealed trait lBT[+A]
  case object LEmpty extends lBT[Nothing]
  case class LNode[+A](elem:A, left:()=>lBT[A], right:()=>lBT[A]) extends lBT[A]
  
 
  def breadthSearch [A](startNode: lBT[A]):Stream[A] ={
  	def search (visited:List[lBT[A]], queue:List[lBT[A]]):Stream[A] =
  		queue match {
  		case Nil => Stream.empty
  		case h::t => if (visited contains h) search (visited, t)
  		  			else	h match{
  				case (LNode(elem,left, right)) => elem#::search ((h::visited), (right()::left()::(t.reverse)).reverse )
  				case LEmpty => search ((visited), (t ) )
  				}
  	}
 	search (List(), List(startNode))
 	}                                         //> breadthSearch: [A](startNode: zad3.lBT[A])Stream[A]
 	
 	val tree = LNode(1,
 					() => LNode(11,
 									()=>	LNode(12,
 										()=>LEmpty, ()=>LEmpty),
 									()=>	LNode(13,() =>LEmpty, ()=>LEmpty)),
 					() => LNode(21, ()=>LEmpty, ()=>LEmpty))
                                                  //> tree  : zad3.LNode[Int] = LNode(1,zad3$$$Lambda$8/2101440631@3b6eb2ec,zad3$$
                                                  //| $Lambda$9/2109957412@1e643faf)
 	breadthSearch(tree) force                 //> res0: scala.collection.immutable.Stream[Int] = Stream(1, 11, 21, 12, 13)


// b)

def lTree (n:Int):lBT[Int]=
 	LNode(n, (() => lTree (2*n)), (() => lTree (2*n+1)))
                                                  //> lTree: (n: Int)zad3.lBT[Int]
 lTree (3)                                        //> res1: zad3.lBT[Int] = LNode(3,zad3$$$Lambda$24/1684106402@17550481,zad3$$$La
                                                  //| mbda$25/2017354584@735f7ae5)
 	}