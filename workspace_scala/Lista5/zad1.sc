object zad1 {
  def lpowiel[A](n: Int, nlist: Stream[A]): Stream[A] = {
    def powiel2[A](el: A, t: Stream[A]): Stream[A] = {
      def powiel3(el: A, count: Int): Stream[A] =
        if (count == 0) lpowiel(n, t)
        else el #:: (powiel3(el, count - 1))
      powiel3(el, n)
    }
    nlist match {
      case Stream.Empty => Stream.Empty
      case h #:: t      => powiel2(h, t)
    }
  }                                               //> lpowiel: [A](n: Int, nlist: Stream[A])Stream[A]
  
  val r1= lpowiel(2,Stream(1,2,3))                //> r1  : Stream[Int] = Stream(1, ?)
  r1 force                                        //> res0: scala.collection.immutable.Stream[Int] = Stream(1, 1, 2, 2, 3, 3)
  
}