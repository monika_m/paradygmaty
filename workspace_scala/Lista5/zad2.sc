object zad2 {
 
def lfib (n:Int):Stream[Int]={
	def fib (n:Int, f1:Int,f2:Int):Stream[Int]=
		if (n==0) Stream.Empty
		else if (n==1) Stream(f1)
		else f1 #::fib(n-1, f2, f1+f2)
	fib(n,0,1)
}                                                 //> lfib: (n: Int)Stream[Int]

val a= lfib (5)                                   //> a  : Stream[Int] = Stream(0, ?)
a force                                           //> res0: scala.collection.immutable.Stream[Int] = Stream(0, 1, 1, 2, 3)
}