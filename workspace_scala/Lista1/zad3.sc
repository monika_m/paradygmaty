object zad3 {
   def replicate[A] (x: A, n: Int): List[A] =
   		if (n>0) x::replicate(x, n-1) else List()
                                                  //> replicate: [A](x: A, n: Int)List[A]
   	
   	replicate('a', 5)                         //> res0: List[Char] = List(a, a, a, a, a)
   	
   	/*def replicate[A] (x: A, n: Int): List[A] =
   		if (n>0) List[A](x):::replicate(x, n-1) else List() */
}