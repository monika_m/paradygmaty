object zad4 {
  def sqrList(xs: List[Int]): List[Int] =
  		if (xs.length>0) List(xs.head*xs.head):::sqrList(xs.tail) else List()
                                                  //> sqrList: (xs: List[Int])List[Int]
    sqrList(List[Int](1,2,-15,0))                 //> res0: List[Int] = List(1, 4, 225, 0)
}