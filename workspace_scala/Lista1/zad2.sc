object zad2 {
   def count[A](x:A, list: List[A]):Int=
   	if (list==Nil) 0 else if (list.head==x) 1 + count(x, list.tail) else 0 + count(x,list.tail)
                                                  //> count: [A](x: A, list: List[A])Int
   val list1 = 'a'::'b'::'a'::Nil
   count('a', list1)
   count('b', list1)
   count('c', list1)
   val list0 = List[Int]()
   count(1, list0) 
   //scala jest taka, żeby nie zmuszać programisty do pisania zbędnych nawiasów, po co ma niszczyć klawiaturę"
}