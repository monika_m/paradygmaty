object zad6 {
   def listLength[A](xs: List[A]):Int=
   if(xs==List()) 0
   else 1+ listLength(xs.tail)                    //> listLength: [A](xs: List[A])Int
   
   listLength(List())                             //> res0: Int = 0
   listLength(List(1))                            //> res1: Int = 1
   listLength(List('a', 'b', 'c'))                //> res2: Int = 3
}