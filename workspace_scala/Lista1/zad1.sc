object zad1 {

def flatten [A](xss: List[List[A]]):List[A]=
		if (xss.length == 0) List()
		else xss.head:::flatten(xss.tail) //> flatten: [A](xss: List[List[A]])List[A]
								//++ też działa, ale zaczyna na odwrót
	
		val list1 = 1::2::3::Nil          //> list1  : List[Int] = List(1, 2, 3)
		val list2 = 4::5::Nil             //> list2  : List[Int] = List(4, 5)
		val list3 = 6::7::Nil             //> list3  : List[Int] = List(6, 7)
		val list =List(list1,list2,list3) //> list  : List[List[Int]] = List(List(1, 2, 3), List(4, 5), List(6, 7))
		flatten (list)                    //> res0: List[Int] = List(1, 2, 3, 4, 5, 6, 7)
}