object zad3 {
  def makesTriangle (xa:Int,ya:Int,xb:Int,yb:Int,xc:Int, yc:Int):Boolean={
  		def line (x1:Int,y1:Int,x2:Int,y2:Int):Int= (y2-y1)/(x2-x1)
  		if (line(xa,ya,xb,yb)==line(xb,yb,xc,yc)) false else true
  }                                               //> makesTriangle: (xa: Int, ya: Int, xb: Int, yb: Int, xc: Int, yc: Int)Boolean
                                                  //| 
 	makesTriangle(0,0,1,1,2,2)                //> res0: Boolean = false
 	makesTriangle(0,0,1,1,0,2)                //> res1: Boolean = true
 	makesTriangle(0,2,3,4,10,-3)              //> res2: Boolean = true
}