object zad2 {
  def fib1 (n:Int):Int =
	if (n==0) 0 else if (n==1) 1 else fib1(n-1)+fib1(n-2) //zlozonosc 2^n
                                                  //> fib1: (n: Int)Int
	
	def fib2 (n:Int):Int ={
	 def fibb (n:Int, act:Int, next:Int):Int =
			if (n==0) act else if (n==1) next else fibb(n-1, next, act+next)
		fibb(n,0,1)							//zlozonosc liniowa
		}                                 //> fib2: (n: Int)Int
}