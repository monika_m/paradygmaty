object zad5 {
  def initSegment [A](xs1:List[A], xs2:List[A]):Boolean =
	if (xs1==List())  true else
		if (xs2==List()) false else
	(xs1, xs2)match{
	 case (h1::t1, h2::t2) => if (h1==h2) initSegment(t1, t2) else false
	 }                                        //> initSegment: [A](xs1: List[A], xs2: List[A])Boolean
	 
	initSegment(List(),List(1))               //> res0: Boolean = true
	initSegment(List('a'), List('a','b'))     //> res1: Boolean = true
	initSegment(List('a'), List('b'))         //> res2: Boolean = false
}