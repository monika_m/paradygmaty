object zad3 {
import scala.annotation.tailrec

  def root3 (a:Double):Double ={
  @tailrec
		def root3check (x:Double):Double =
		if (math.abs(math.pow(x,3)-a) <= (1E-015)*math.abs(a)) x
		else root3check(x +(a/(x*x) -x)/3)
	root3check(if (a <= 1) a else a /3)
	}                                         //> root3: (a: Double)Double
	root3(1)                                  //> res0: Double = 1.0
	root3(2)                                  //> res1: Double = 1.2599210498948732
	
	 def root3a (a:Double):Double ={
		def root3check (x:Double):Double =
		if (math.abs(math.pow(x,3)-a) <= (1E-015)*math.abs(a)) x
		else root3check(x +(a/(x*x) -x)/3)
	root3check(if (a <= 1) a else a /3)
	}                                         //> root3a: (a: Double)Double
}