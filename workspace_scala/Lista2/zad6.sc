object zad6 {
 	def replaceNth[A](xs:List[A], n:Int, x:A):List[A]=
		(xs, n)match {
			case(List(),_) => List()
			case (h::t, 0) => x::t
			case (h::t, _) => h::replaceNth(t, n-1, x)
	}                                         //> replaceNth: [A](xs: List[A], n: Int, x: A)List[A]
	
	replaceNth(List(1,2,3),2,10)              //> res0: List[Int] = List(1, 2, 10)
}