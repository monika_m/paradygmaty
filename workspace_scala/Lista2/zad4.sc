object zad4 {
  val a = (-2,-1,0,1,2)                           //> a  : (Int, Int, Int, Int, Int) = (-2,-1,0,1,2)
  
  val (_,_,x,_,_) = a                             //> x  : Int = 0

	val b =  ((1,2), (0,1))                   //> b  : ((Int, Int), (Int, Int)) = ((1,2),(0,1))
	val (_,(y,_)) = b                         //> y  : Int = 0
}