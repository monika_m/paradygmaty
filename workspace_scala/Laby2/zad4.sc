object zad4 {
  def zip[A](l1:List[A], l2:List[A]):List[A]=
   	if (l1==List()) l2 else if (l2==List()) l1 else List(l1.head,l2.head):::zip(l1.tail, l2.tail)
                                                  //> zip: [A](l1: List[A], l2: List[A])List[A]
 zip(List(1,3,5),List(2,4))                       //> res0: List[Int] = List(1, 2, 3, 4, 5)
 zip(List(),List(1,2))                            //> res1: List[Int] = List(1, 2)
 zip(List('a','b','c'), List())                   //> res2: List[Char] = List(a, b, c)
}