object zad3_3 {
  def unZigi(list:List[Double]):Double={
  	def isIn (x:Double, temp:List[Double]):Boolean=
  			if (temp == Nil) false
  			else if (temp.head==x) true else isIn(x, temp.tail)
  	def count(x:Double, list: List[Double]):Int=
   			if (list==Nil) 0
   			else if (list.head==x) 1 + count(x, list.tail) else 0 + count(x,list.tail)
  	def check (list:List[Double], temp:List[Double]):Double=
  			//if (list == Nil) 0 else
  			if (isIn(list.head, temp)) check(list.tail, temp) //head byl juz sprawdzany
  				else if (count(list.head, list)%2 !=0) list.head
  						else check (list.tail, list.head::temp)
  	check(list, List())
  	}                                         //> unZigi: (list: List[Double])Double
  	
  	unZigi (List(1.0, 1.0, 2.0))              //> res0: Double = 2.0
  	unZigi (List(1.0, 1.0, 1.0, 2.0, 2.0))    //> res1: Double = 1.0
  	unZigi (List(1.0, 35.0, 1.0, 40.0, 35.0, 1.0, 2.0,40.0, 2.0))
                                                  //> res2: Double = 1.0
}