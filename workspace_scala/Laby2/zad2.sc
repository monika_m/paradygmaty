object zad2 {

	def fib1 (n:Int):Int =
		if (n==0) 0 else if (n==1) 1 else fib1(n-1)+fib1(n-2)
                                                  //> fib1: (n: Int)Int

  def fib (n:Int):Int ={
	 def fibb (n:Int, f1:Int, f2:Int):Int =
			if (n==0) f1 else if (n==1) f2 else fibb(n-1, f2, f1+f2)
		fibb(n,0,1)
		}                                 //> fib: (n: Int)Int
		
	fib(0)                                    //> res0: Int = 0
	fib(1)                                    //> res1: Int = 1
	fib(2)                                    //> res2: Int = 1
	fib(3)                                    //> res3: Int = 2
	fib(4)                                    //> res4: Int = 3
	fib(5)                                    //> res5: Int = 5
	fib(42)                                   //> res6: Int = 267914296
	fib1(1)                                   //> res7: Int = 1
	fib1(3)                                   //> res8: Int = 2
	fib1(5)                                   //> res9: Int = 5
	fib1(42)                                  //> res10: Int = 267914296
}