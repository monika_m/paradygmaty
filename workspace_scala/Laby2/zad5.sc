object zad5 {
  
  def bucket3(tab: Array[Int], n:Int, list:List[Int]):List[Int]=
  	if (list.length==10) list
  	else if (n<0) list
  	else if (tab(n)>0) {tab(n)=tab(n)-1
  	bucket3(tab,n,list:::List(n))}
  	else bucket3(tab, n-1, list) //if (tab(n)==0)
                                                  //> bucket3: (tab: Array[Int], n: Int, list: List[Int])List[Int]
  	
  
  def bucket(list:List[Int]):List[Int]={
  	def bucket2(list: List[Int], tab:Array[Int]):List[Int]=
  		if(list==Nil) bucket3(tab, 100, List())
  		else  {tab(list.head)=(tab(list.head)+1)
  		bucket2(list.tail, tab)}
  		val tab0=Array.fill(101){0}
  	bucket2(list, tab0)
  }                                               //> bucket: (list: List[Int])List[Int]
  bucket(List(1,2,3,4,5,6,7,8,9,0,11,23,100,0,11,99,100,89, 12, 0, 99, 80))
                                                  //> res0: List[Int] = List(100, 100, 99, 99, 89, 80, 23, 12, 11, 11)
  
}