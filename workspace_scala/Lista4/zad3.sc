object zad3 {

sealed trait Tree[+A]
case class Empty[A]() extends Tree[A]
case class Node[A](elem:A, left:Tree[A], right:Tree[A]) extends Tree[A]

def  breadthSearch[A] (tree:Tree[A]):List[A] = {
	def search (visited:List[A], node:Tree[A]):List[A]=
		node match{
		case Empty() => visited 
		case Node(v, left, right) => if (visited contains v) search ((search (visited, left) ), right)
															else search ((search (visited:::List(v), left)), right)
		}
	search (List(), tree)
}                                                 //> breadthSearch: [A](tree: zad3.Tree[A])List[A]
val tt = Node(1, Node(2, Node(4, Empty(), Empty() ), Empty() ), Node(3, Node(5, Empty(), Node(6, Empty(), Empty() ) ), Empty() ) )
                                                  //> tt  : zad3.Node[Int] = Node(1,Node(2,Node(4,Empty(),Empty()),Empty()),Node(3
                                                  //| ,Node(5,Empty(),Node(6,Empty(),Empty())),Empty()))

breadthSearch (tt)                                //> res0: List[Int] = List(1, 2, 4, 3, 5, 6)
//"oczywiście w Ocamlu analogicznie"
//zad 4
def intPath[A] (tree:Tree[A]):Int = {
	def search (node:Tree[A], path:Int):Int=
		node match {
		case Empty() => 0
		case Node(_, left, right) => path+ search (left, path+1) + search (right, path+1)
	}
	search (tree, 0)
}                                                 //> intPath: [A](tree: zad3.Tree[A])Int
intPath (tt)                                      //> res1: Int = 9

def extPath[A] (tree:Tree[A]):Int = {
	def search (node:Tree[A], path:Int):Int=
		node match {
		case Empty() => path
		case Node(_, left, right) => search (left, path+1) + search (right, path+1)
		}
		search (tree, 0)
}                                                 //> extPath: [A](tree: zad3.Tree[A])Int
extPath (tt)                                      //> res2: Int = 21
}