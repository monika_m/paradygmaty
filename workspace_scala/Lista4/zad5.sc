object zad5 {
 
sealed trait Graphs[+A]
case class Graph[A](succ: A=>List[A]) extends Graphs[A]

val g = Graph((i: Int) =>
			i match {
			case 0 => List(3)
			case 1 => List(0,2,4)
			case 2 => List(1)
			case 3 => Nil
			case 4 => List(0,2)
			case n => throw new Exception("Graph g: node " + n + " doesn't exist")
			})                        //> g  : zad5.Graph[Int] = Graph(zad5$$$Lambda$3/812265671@41a4555e)
			
def depthSearch[A](g: Graph[A], el:A):List[A]={
	def search (els: List[A], visited:List[A]):List[A] =
		els match {
		case Nil => List()
		case h::t => if (visited contains h) search (t, visited)
							else h::(search (((g succ h):::t), (h::visited)))
		}
	 search (List(el), List())
	}                                         //> depthSearch: [A](g: zad5.Graph[A], el: A)List[A]
depthSearch (g, 4)                                //> res0: List[Int] = List(4, 0, 3, 2, 1)

}