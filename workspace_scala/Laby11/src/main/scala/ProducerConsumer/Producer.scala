

package ProducerConsumer

import akka.actor._
class Producer(consumer: ActorRef) extends Actor {
  override def receive = {
    case "Start" => produce(10)
    case _       => println("I don't understand")
  }

  def produce(number: Int): Unit = {
    for (i <- 1 to number){
      println(s"Producer puts m$i")
      consumer ! Consumer.Message(s"m$i")
    }
    println("Producer puts Done")
    consumer ! Consumer.Message("Done")

  }
}
object Main extends App {
  val mainSystem = ActorSystem("ProdCons")
  val consumer = mainSystem.actorOf(Props(classOf[Consumer]))
  val producer = mainSystem.actorOf(Props(classOf[Producer], consumer))
  producer ! "Start"
  
  //mainSystem.terminate()
}