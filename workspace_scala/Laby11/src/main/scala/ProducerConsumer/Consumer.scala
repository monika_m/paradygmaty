

package ProducerConsumer

import akka.actor.Actor

class Consumer extends Actor{
   override def receive = {
     case Consumer.Message("Done") => {
       println("Consumer got Done")
       context.system.terminate()
     }
     case Consumer.Message(msg) => println(s"Consumer takes $msg")
     case _ => println("Error")
  }
}
object Consumer {
  case class Message(kom: String)
}