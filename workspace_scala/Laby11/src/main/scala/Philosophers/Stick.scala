package Philosophers

import akka.actor._

class Stick(private[this] var used:Boolean=false) extends Actor{
   override def receive = {
     case Stick.Msg("get")=> {
       if (used) sender() ! Stick.Msg("used")
       else {
         used=true;
         sender() ! Stick.Msg("ok")
       }
     }
     case Stick.Msg("let")=> used=false
     case _ => println("Stick Error")
   }
}

object Stick{
  case class Msg(msg:String)
}

