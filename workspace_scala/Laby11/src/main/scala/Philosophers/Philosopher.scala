package Philosophers

import akka.actor._

class Philosopher(private val id: Int, private[this] var itersLeft: Int, var sticks:Array[ActorRef], private val controller:ActorRef, private var gotOne:Boolean=false) extends Actor {

  override def receive = {
    case Philosopher.Msg("think") => {
      println(s"Philosopher $id thinking")
      Thread.sleep(1000)
      itersLeft = itersLeft - 1;
      if (itersLeft > 0)
        self ! Philosopher.Msg("eat")
      else {
        controller ! Stick.Msg("end")
      }
    }
    case Philosopher.Msg("eat") =>{
      if (id< (id+1)%5){
        //println(s"Philosopher $id wants to eat stick $id")
        sticks(id) ! Stick.Msg("get")
      } else {
       // println(s"Philosopher $id wants to eat 2 stick ($id+1)%5")
        sticks((id+1)%5) ! Stick.Msg("get")
      }
    }
    case Stick.Msg("used")=>{
      //println(s"Philosopher $id couldnt get stick $sender()")
      Thread.sleep(200)
      sender() ! Stick.Msg("get")
    }
    case Stick.Msg("ok")=>{
      if (!gotOne){
        gotOne = true;
        if (id!=4)
        sticks((id+1)%5) ! Stick.Msg("get")
        else 
           sticks(id) ! Stick.Msg("get")
      }
      else {
        println(s"Philosopher $id eating")
        Thread.sleep(1000)
        sticks(id) ! Stick.Msg("let")
        sticks((id+1)%5) ! Stick.Msg("let")
        gotOne=false;
        self ! Philosopher.Msg("think")
      }
    }
    case _ => println("Phil Error")
  }
}

object Philosopher{
  case class Msg(msg:String)
}