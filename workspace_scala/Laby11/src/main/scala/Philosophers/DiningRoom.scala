
package Philosophers

import akka.actor._

class DiningRoom (private var done:Int=0) extends Actor {
   override def receive = {
     case Stick.Msg("end") => {
       done=done+1;
       if (done==5)
         context.system.terminate()
     }
     case _ => println("Wrong")
   }
}

object Main extends App{
  val mainSystem = ActorSystem("Philosophers")
  val controller = mainSystem.actorOf(Props(classOf[DiningRoom], 0))
  val sticks = new Array[ActorRef](5)
  for (i <- 0 to 4){
    sticks(i) = mainSystem.actorOf(Props(classOf[Stick], false))
  }
  val philosophers = new Array[ActorRef](5)
  for (i <- 0 to 4){
    philosophers(i) = mainSystem.actorOf(Props(classOf[Philosopher], i, 5, sticks, controller, false))
    philosophers(i) ! Philosopher.Msg("think")
  }
}