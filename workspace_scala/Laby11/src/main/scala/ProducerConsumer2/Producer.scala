

package ProducerConsumer2

import akka.actor._
class Producer2(buffer: ActorRef) extends Actor {
  override def receive = {
    case "Start" => produce(10)
    case _       => println("I don't understand")
  }

  def produce(number: Int): Unit = {
    for (i <- 1 to number)
      buffer ! s"m$i"
    buffer ! "Done"

  }
}
object Main2 extends App {
  val mainSystem = ActorSystem("ProdCons")
  val consumer = mainSystem.actorOf(Props(classOf[Consumer2]))
  val buffer = mainSystem.actorOf(Props(classOf[Buffer2], consumer))
  val producer = mainSystem.actorOf(Props(classOf[Producer2], buffer))
  producer ! "Start"
  
  //mainSystem.terminate()
}