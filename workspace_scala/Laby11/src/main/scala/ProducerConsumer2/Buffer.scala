

package ProducerConsumer2

import akka.actor._

class Buffer2(consumer:ActorRef) extends Actor{

  override def receive = {
    case msg:String => consumer ! Buffer2.Message(msg)
    case _ => println("Something went wrong")
    }
}
object Buffer2 {
  case class Message(kom: String)
}