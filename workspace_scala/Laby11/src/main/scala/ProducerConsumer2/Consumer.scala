

package ProducerConsumer2

import akka.actor.Actor

class Consumer2 extends Actor{
   override def receive = {
     case Buffer2.Message("Done") => context.system.terminate()
     case Buffer2.Message(msg) => println(msg)
     case _ => println("Error")
  }
}